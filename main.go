package main

import (
	"fmt"

	"./src/Controladores/ProductoControler"
	//"./src/Controladores/UnidadControler"

	"./src/Modulos/Variables"

	iris "gopkg.in/kataras/iris.v6"
	"gopkg.in/kataras/iris.v6/adaptors/httprouter"
)

func main() {

	app := iris.New()
	app.Adapt(httprouter.New())

	app.Set(iris.OptionCharset("UTF-8"))

	var DataCfg = MoVar.CargaSeccionCFG(MoVar.SecDefault)

	//###################### Catalogo ################################

	//###################### Producto ################################
	//Index (Búsqueda)
	app.Get("/Productos/GetAll", ProductoControler.GetAll)
	app.Get("/Productos/CountAll", ProductoControler.CountAll)
	app.Get("/Productos/GetOne/:ID", ProductoControler.GetOne)
	app.Get("/Productos/BuscarEnElastic/:Cadena", ProductoControler.BuscarEnElastic)
	app.Post("/Productos/GetEspecifics", ProductoControler.GetEspecifics)
	app.Post("/Productos/GetProductoPorCampo", ProductoControler.GetEspecificByFields)
	app.Post("/Productos/GetProductoPorCampoElastic", ProductoControler.ElasticGetEspecificByFields)
	app.Post("/Productos/GetIDPorCampo", ProductoControler.GetIDByField)

	//################# MÉTODOS DE CONSUMO ###########################

	if DataCfg.Puerto != "" {
		fmt.Println("Ejecutandose en el puerto: ", DataCfg.Puerto)
		fmt.Println("Acceder a la siguiente url: ", DataCfg.BaseURL)
		app.Listen(":" + DataCfg.Puerto)
	} else {
		fmt.Println("Ejecutandose en el puerto: 8080")
		fmt.Println("Acceder a la siguiente url: localhost")
		app.Listen(":8080")
	}

}
