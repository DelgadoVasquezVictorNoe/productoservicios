package ProductoControler

import (
	"encoding/json"
	"fmt"
	"time"

	"../../Modelos/ProductoModel"

	"../../Modulos/Conexiones"
	"../../Modulos/Crypto"
	"../../Modulos/Estructuras"
	"../../Modulos/General"
	"../../Modulos/Variables"

	"gopkg.in/kataras/iris.v6"
	"gopkg.in/mgo.v2/bson"
)

//DataM exporta las variables de la conexion de mongo
var DataM = MoVar.CargaSeccionCFG(MoVar.SecMongo)

//GetAll regresa todos los productos en la base de datos.
func GetAll(ctx *iris.Context) {

	//#################################################################################################
	var resultado map[string]interface{}

	conn, err := MoConexion.MongoGetSession()
	if err != nil {
		fmt.Println(err)
	}
	//defer conn.Close()

	err = conn.DB(DataM.NombreBase).Run(bson.M{"eval": fmt.Sprintf(`%s([]);`, "getProductosWS")}, &resultado)

	//productos, err := ProductoModel.GetAll()
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar los productos")
		if err != nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	if len(resultado["retval"].([]interface{})) != 0 {

		var Doc MoEstructuras.DocAnswer

		Doc.Header.From = "Modelo Producto"
		Doc.Header.Process = "GetProductos_WS"
		Doc.Header.Date = time.Now()
		Doc.Header.Status = true
		Doc.Header.Msg = "Registros encontrados"

		Doc.Body.When = time.Now()
		Doc.Body.Answer = true
		Doc.Body.Content, _ = json.Marshal(resultado["retval"])

		ToSend, err := Crypto.CodificaSend(Doc)
		if err != nil {
			Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
			if err != nil {
				Data, _ := json.Marshal(Send)
				ctx.Header().Set("Content-Type", "application/json")
				ctx.SetStatusCode(iris.StatusInternalServerError)
				ctx.Write(Data)
				return
			}
		}

		ctx.JSON(iris.StatusOK, ToSend)
		return

	}
	fmt.Println("No hay registros")
	Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "No hay registros para mostrar")
	if err == nil {
		Data, _ := json.Marshal(Send)
		ctx.Header().Set("Content-Type", "application/json")
		ctx.SetStatusCode(iris.StatusInternalServerError)
		ctx.Write(Data)
		return
	}

}

//CountAll regresa el total de productos en la base de datos.
func CountAll(ctx *iris.Context) {

	//#################################################################################################

	total, err := ProductoModel.CountAll()
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar el total de productos")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	} else {
		var Doc MoEstructuras.DocAnswer

		Doc.Header.From = "Producto"
		Doc.Header.Process = "CountAll_WS"
		Doc.Header.Date = time.Now()
		Doc.Header.Status = true
		Doc.Header.Msg = "Consulta exitosa"

		Doc.Body.When = time.Now()
		Doc.Body.Answer = true
		Doc.Body.Content, _ = json.Marshal(total)

		ToSend, err := Crypto.CodificaSend(Doc)
		if err != nil {
			Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
			if err == nil {
				Data, _ := json.Marshal(Send)
				ctx.Header().Set("Content-Type", "application/json")
				ctx.SetStatusCode(iris.StatusInternalServerError)
				ctx.Write(Data)
				return
			}
		}

		ctx.JSON(iris.StatusOK, ToSend)
		return
	}

}

//GetOne regresa un producto de la base de datos.
func GetOne(ctx *iris.Context) {

	idProducto := ctx.Param("ID")
	//#################################################################################################

	var producto map[string]interface{}

	conn, err := MoConexion.MongoGetSession()
	if err != nil {
		fmt.Println(err)
	}
	//defer conn.Close()
	err = conn.DB(DataM.NombreBase).Run(bson.M{"eval": fmt.Sprintf(`%s([ObjectId('%s')]);`, "getProductosWS", idProducto)}, &producto)

	//producto, err := ProductoModel.GetOne(bson.ObjectIdHex(idProducto))
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar el producto")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	if len(producto["retval"].([]interface{})) != 0 {

		var Doc MoEstructuras.DocAnswer

		Doc.Header.From = "Modelo Producto"
		Doc.Header.Process = "GetProductos_WS"
		Doc.Header.Date = time.Now()
		Doc.Header.Status = true
		Doc.Header.Msg = "Producto encontrado"

		Doc.Body.When = time.Now()
		Doc.Body.Answer = true
		Doc.Body.Content, _ = json.Marshal(producto["retval"].([]interface{})[0])

		ToSend, err := Crypto.CodificaSend(Doc)
		if err != nil {
			Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
			if err == nil {
				Data, _ := json.Marshal(Send)
				ctx.Header().Set("Content-Type", "application/json")
				ctx.SetStatusCode(iris.StatusInternalServerError)
				ctx.Write(Data)
				return
			}
		}

		ctx.JSON(iris.StatusOK, ToSend)
		return

	}
	fmt.Println("No se encontro el producto")
	Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "No se encontro el producto")
	if err == nil {
		Data, _ := json.Marshal(Send)
		ctx.Header().Set("Content-Type", "application/json")
		ctx.SetStatusCode(iris.StatusInternalServerError)
		ctx.Write(Data)
		return
	}

}

//GetEspecifics regresa un conjunto de productos especificos de la base de datos.
func GetEspecifics(ctx *iris.Context) {
	//#################################################################################################
	Document, err := MoGeneral.PrepareDocRequest(ctx)
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar el producto")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	From := Document.Header.From
	//#################################################################################################

	if !MoGeneral.EstaVacio(Document) {

		var Ides []bson.ObjectId
		err = json.Unmarshal(Document.Body.Content, &Ides)

		if len(Ides) != 0 {
			array := ``
			array = `[`
			for _, v := range Ides {
				array += fmt.Sprintf(`ObjectId('%s'),`, v.Hex())
			}
			array = array[0 : len(array)-1]
			array += `]`

			fmt.Println(array)

			var productos map[string]interface{}

			conn, err := MoConexion.MongoGetSession()
			if err != nil {
				fmt.Println(err)
			}
			//defer conn.Close()
			err = conn.DB(DataM.NombreBase).Run(bson.M{"eval": fmt.Sprintf(`%s(%s);`, "getProductosWS", array)}, &productos)

			//productos, err := ProductoModel.GetEspecifics(Ides)
			if err != nil {
				Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar los productos")
				if err == nil {
					Data, _ := json.Marshal(Send)
					ctx.Header().Set("Content-Type", "application/json")
					ctx.SetStatusCode(iris.StatusInternalServerError)
					ctx.Write(Data)
					return
				}
			}

			if len(productos["retval"].([]interface{})) != 0 {

				fmt.Println(productos["retval"])

				var Doc MoEstructuras.DocAnswer

				Doc.Header.From = "Modelo Producto"
				Doc.Header.Process = "GetEspecifics_WS"
				Doc.Header.Date = time.Now()
				Doc.Header.Status = true
				Doc.Header.Msg = "Registros encontrados"

				Doc.Body.When = time.Now()
				Doc.Body.Answer = true
				Doc.Body.Content, _ = json.Marshal(productos["retval"])

				ToSend, err := Crypto.CodificaSend(Doc)
				if err != nil {
					Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
					if err == nil {
						Data, _ := json.Marshal(Send)
						ctx.Header().Set("Content-Type", "application/json")
						ctx.SetStatusCode(iris.StatusInternalServerError)
						ctx.Write(Data)
						return
					}
				}

				ctx.JSON(iris.StatusOK, ToSend)
				return
			}
			fmt.Println("No se encontraron productos")
			Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se encontraron productos")
			if err == nil {
				Data, _ := json.Marshal(Send)
				ctx.Header().Set("Content-Type", "application/json")
				ctx.SetStatusCode(iris.StatusInternalServerError)
				ctx.Write(Data)
				return
			}
		}
		fmt.Println("Arreglo vacío")
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibio ninguna informacion de los productos")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}
	Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibió documento")
	if err == nil {
		Data, _ := json.Marshal(Send)
		ctx.Header().Set("Content-Type", "application/json")
		ctx.SetStatusCode(iris.StatusInternalServerError)
		ctx.Write(Data)
		return
	}
}

//GetEspecificByFields regresa un documento de Mongo especificando un campo y un determinado valor
func GetEspecificByFields(ctx *iris.Context) {
	//#################################################################################################
	Document, err := MoGeneral.PrepareDocRequest(ctx)
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al obtener el documento ")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	From := Document.Header.From
	//#################################################################################################

	if !MoGeneral.EstaVacio(Document) {
		var datos map[string]interface{}

		err = json.Unmarshal(Document.Body.Content, &datos)
		//fmt.Println(datos)

		campo := datos["campo"].(string)
		valor := datos["valor"].(string)

		if campo != "" && valor != "" {

			productos, err := ProductoModel.GetEspecificByFields(campo, valor)
			if err != nil {
				Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar el producto")
				if err == nil {
					Data, _ := json.Marshal(Send)
					ctx.Header().Set("Content-Type", "application/json")
					ctx.SetStatusCode(iris.StatusInternalServerError)
					ctx.Write(Data)
					return
				}
			}

			var Doc MoEstructuras.DocAnswer
			if !MoGeneral.EstaVacio(productos) {

				var producto map[string]interface{}

				conn, err := MoConexion.MongoGetSession()
				if err != nil {
					fmt.Println(err)
				}
				//defer conn.Close()
				err = conn.DB(DataM.NombreBase).Run(bson.M{"eval": fmt.Sprintf(`%s([ObjectId('%s')]);`, "getProductosWS", productos.ID.Hex())}, &producto)

				fmt.Println("Entro una peticion", producto["retval"])

				Doc.Header.From = "Modelo Producto"
				Doc.Header.Process = "GetProductoByCampo_WS"
				Doc.Header.Date = time.Now()
				Doc.Header.Status = true
				Doc.Header.Msg = "Registros encontrados"

				Doc.Body.When = time.Now()
				Doc.Body.Answer = true
				Doc.Body.Content, _ = json.Marshal(producto["retval"].([]interface{})[0])
			} else {
				fmt.Println("No se encontraron productos")

				Doc.Header.From = "Modelo Producto"
				Doc.Header.Process = "GetProductoByCampo_WS"
				Doc.Header.Date = time.Now()
				Doc.Header.Status = true
				Doc.Header.Msg = "Registros no encontrados"

				Doc.Body.When = time.Now()
				Doc.Body.Answer = true

				var Producto ProductoModel.ProductoMgo
				Doc.Body.Content, _ = json.Marshal(Producto)
			}
			ToSend, err := Crypto.CodificaSend(Doc)
			if err != nil {
				Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
				if err == nil {
					Data, _ := json.Marshal(Send)
					ctx.Header().Set("Content-Type", "application/json")
					ctx.SetStatusCode(iris.StatusInternalServerError)
					ctx.Write(Data)
					return
				}
			}

			ctx.JSON(iris.StatusOK, ToSend)
			return

		}
		fmt.Println("Datos vacíos")
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibio la suficiente informacion para buscar el producto")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}
	Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibió documento")
	if err == nil {
		Data, _ := json.Marshal(Send)
		ctx.Header().Set("Content-Type", "application/json")
		ctx.SetStatusCode(iris.StatusInternalServerError)
		ctx.Write(Data)
		return
	}
}

//GetIDByField regresa un documento específico de Mongo (Por Coleccion)
func GetIDByField(ctx *iris.Context) {
	//#################################################################################################
	Document, err := MoGeneral.PrepareDocRequest(ctx)
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al obtener el documento ")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	From := Document.Header.From
	//#################################################################################################

	if !MoGeneral.EstaVacio(Document) {

		var datos map[string]interface{}

		err = json.Unmarshal(Document.Body.Content, &datos)

		//log.Fatal(datos)

		campo := datos["campo"].(string)
		valor := datos["valor"].(string)

		if campo != "" && valor != "" {

			productoid, err := ProductoModel.GetIDByField(campo, valor)
			if err != nil {
				Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar los productos")
				if err == nil {
					Data, _ := json.Marshal(Send)
					ctx.Header().Set("Content-Type", "application/json")
					ctx.SetStatusCode(iris.StatusInternalServerError)
					ctx.Write(Data)
					return
				}
			}

			if bson.IsObjectIdHex(productoid.Hex()) {
				var Doc MoEstructuras.DocQuery

				Doc.Header.From = "Modelo Producto"
				Doc.Header.Process = "GetIDByCampo_WS"
				Doc.Header.Date = time.Now()
				Doc.Header.Status = true
				Doc.Header.Msg = "Registros encontrados"

				Doc.Body.When = time.Now()

				Doc.Body.Content, _ = json.Marshal(productoid)

				ToSend, err := Crypto.CodificaSend(Doc)
				if err != nil {
					Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
					if err == nil {
						Data, _ := json.Marshal(Send)
						ctx.Header().Set("Content-Type", "application/json")
						ctx.SetStatusCode(iris.StatusInternalServerError)
						ctx.Write(Data)
						return
					}
				}

				ctx.JSON(iris.StatusOK, ToSend)
				return
			}
			fmt.Println("No se encontraron productos")
			Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se encontraron productos")
			if err == nil {
				Data, _ := json.Marshal(Send)
				ctx.Header().Set("Content-Type", "application/json")
				ctx.SetStatusCode(iris.StatusInternalServerError)
				ctx.Write(Data)
				return
			}
		}
		fmt.Println("Datos vacíos")
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibio la suficiente informacion para buscar el producto")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}
	Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibió documento")
	if err == nil {
		Data, _ := json.Marshal(Send)
		ctx.Header().Set("Content-Type", "application/json")
		ctx.SetStatusCode(iris.StatusInternalServerError)
		ctx.Write(Data)
		return
	}
}

//ElasticGetEspecificByFields regresa un documento de Elastic especificando un campo y un determinado valor
func ElasticGetEspecificByFields(ctx *iris.Context) {
	//#################################################################################################
	Document, err := MoGeneral.PrepareDocRequest(ctx)
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al obtener el documento ")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	From := Document.Header.From
	//#################################################################################################

	if !MoGeneral.EstaVacio(Document) {

		var datos map[string]interface{}
		err = json.Unmarshal(Document.Body.Content, &datos)

		campo := datos["campo"].(string)
		valor := datos["valor"].(string)

		if campo != "" && valor != "" {

			docs, err := MoConexion.ElasticGetEspecificByFields(MoVar.IndexElastic, "Producto", valor, campo)
			var Producto ProductoModel.ProductoElastic
			for _, v := range docs.Hits.Hits {
				err := json.Unmarshal(*v.Source, &Producto)
				if err != nil {
					fmt.Println("Unmarchall Error: ", err)
				}
			}

			if err != nil {
				Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar el producto")
				if err == nil {
					Data, _ := json.Marshal(Send)
					ctx.Header().Set("Content-Type", "application/json")
					ctx.SetStatusCode(iris.StatusInternalServerError)
					ctx.Write(Data)
					return
				}
			}

			if !MoGeneral.EstaVacio(Producto) {

				var Doc MoEstructuras.DocAnswer

				Doc.Header.From = "Modelo Producto"
				Doc.Header.Process = "GetProductoByCampo_WS"
				Doc.Header.Date = time.Now()
				Doc.Header.Status = true
				Doc.Header.Msg = "Registros encontrados"

				Doc.Body.When = time.Now()
				Doc.Body.Answer = true
				Doc.Body.Content, _ = json.Marshal(Producto)

				ToSend, err := Crypto.CodificaSend(Doc)
				if err != nil {
					Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
					if err == nil {
						Data, _ := json.Marshal(Send)
						ctx.Header().Set("Content-Type", "application/json")
						ctx.SetStatusCode(iris.StatusInternalServerError)
						ctx.Write(Data)
						return
					}
				}

				ctx.JSON(iris.StatusOK, ToSend)
				return
			}
			fmt.Println("No se encontraron productos")
			Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se encontraron productos")
			if err == nil {
				Data, _ := json.Marshal(Send)
				ctx.Header().Set("Content-Type", "application/json")
				ctx.SetStatusCode(iris.StatusInternalServerError)
				ctx.Write(Data)
				return
			}
		}
		fmt.Println("Datos vacíos")
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibio la suficiente informacion para buscar el producto")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}
	Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibió documento")
	if err == nil {
		Data, _ := json.Marshal(Send)
		ctx.Header().Set("Content-Type", "application/json")
		ctx.SetStatusCode(iris.StatusInternalServerError)
		ctx.Write(Data)
		return
	}
}

//BuscarEnElastic regresa un documento de Elastic con el resultado de la busqueda
func BuscarEnElastic(ctx *iris.Context) {

	cadena := ctx.Param("Cadena")

	//cadena := "tubular"
	//#################################################################################################

	docs, err := ProductoModel.BuscarEnElastic(cadena)

	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al realizar la busqueda en elastic")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	var Doc MoEstructuras.DocAnswer

	Doc.Header.From = "Modelo Producto"
	Doc.Header.Process = "BuscarEnElastic_WS"
	Doc.Header.Date = time.Now()
	Doc.Header.Status = true

	Doc.Body.When = time.Now()
	Doc.Body.Answer = true
	Doc.Body.Content, _ = json.Marshal(docs)

	if docs.Hits.TotalHits > 0 {
		Doc.Header.Msg = "Productos encontrados"
	} else {
		fmt.Println("No se encontraron productos")
		Doc.Header.Msg = "Productos no encontrados"
	}

	ToSend, err := Crypto.CodificaSend(Doc)
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	ctx.JSON(iris.StatusOK, ToSend)
	return

}

//InsertaMgo inserta un nuevo documento de Mongo
/*func InsertaMgo(ctx *iris.Context) {
	//#################################################################################################
	Document, err := MoGeneral.PrepareDocRequest(ctx)
	if err != nil {
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al obtener el documento ")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}

	From := Document.Header.From
	//#################################################################################################

	if !MoGeneral.EstaVacio(Document) {

		var datos map[string]interface{}

		err = json.Unmarshal(Document.Body.Content, &datos)

		EstatusPeticion := false
		var Producto ProductoModel.ProductoMgo

		Producto.ID = bson.NewObjectId()

		nombre := datos["Nombre"].(string)

		if nombre == "" {
			EstatusPeticion = true
		}

		Producto.Nombre = nombre

		codigos := datos["Codigos"].([]string)
		codigosval := datos["Valcodigos"].([]string)
		if len(codigos) > 0 {
			Producto.Codigos.Claves = codigos
			Producto.Codigos.Valores = codigosval
		} else {
			fmt.Println("No trae Códigos")
			EstatusPeticion = true
		}

		tipo := datos["Tipo"].(string)

		if tipo == "" {
			fmt.Println("No especificó Tipo.")
			EstatusPeticion = true
		} else {
			if bson.IsObjectIdHex(tipo) {
				Producto.Tipo = bson.ObjectIdHex(tipo)
			} else {
				EstatusPeticion = true
			}
		}



		if campo != "" && valor != "" {

			productoid, err := ProductoModel.GetIDByField(campo, valor)
			if err != nil {
				Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", "Error al consultar los productos")
				if err == nil {
					Data, _ := json.Marshal(Send)
					ctx.Header().Set("Content-Type", "application/json")
					ctx.SetStatusCode(iris.StatusInternalServerError)
					ctx.Write(Data)
					return
				}
			}

			if bson.IsObjectIdHex(productoid.Hex()) {
				var Doc MoEstructuras.DocQuery

				Doc.Header.From = "Modelo Producto"
				Doc.Header.Process = "GetIDByCampo_WS"
				Doc.Header.Date = time.Now()
				Doc.Header.Status = true
				Doc.Header.Msg = "Registros encontrados"

				Doc.Body.When = time.Now()

				Doc.Body.Content, _ = json.Marshal(productoid)

				ToSend, err := Crypto.CodificaSend(Doc)
				if err != nil {
					Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", "Productos", err.Error())
					if err == nil {
						Data, _ := json.Marshal(Send)
						ctx.Header().Set("Content-Type", "application/json")
						ctx.SetStatusCode(iris.StatusInternalServerError)
						ctx.Write(Data)
						return
					}
				}

				ctx.JSON(iris.StatusOK, ToSend)
				return
			}
			fmt.Println("No se encontraron productos")
			Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se encontraron productos")
			if err == nil {
				Data, _ := json.Marshal(Send)
				ctx.Header().Set("Content-Type", "application/json")
				ctx.SetStatusCode(iris.StatusInternalServerError)
				ctx.Write(Data)
				return
			}
		}
		fmt.Println("Datos vacíos")
		Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibio la suficiente informacion para buscar el producto")
		if err == nil {
			Data, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.SetStatusCode(iris.StatusInternalServerError)
			ctx.Write(Data)
			return
		}
	}
	Send, err := MoGeneral.GetDocErrorToSend(false, "1234578", From, "No se recibió documento")
	if err == nil {
		Data, _ := json.Marshal(Send)
		ctx.Header().Set("Content-Type", "application/json")
		ctx.SetStatusCode(iris.StatusInternalServerError)
		ctx.Write(Data)
		return
	}
}
*/
