package UnidadControler

import (
	"encoding/json"
	"fmt"
	"html/template"
	"strconv"
	"time"

	"../../Modelos/UnidadModel"

	"../../Modulos/CargaCombos"
	"../../Modulos/General"

	"gopkg.in/kataras/iris.v6"
	"gopkg.in/mgo.v2/bson"
)

//##########< Variables Generales > ############

var numeroRegistros int
var numeroRegistrosSat int
var paginasTotales int
var paginasTotalesSat int
var cadenaBusqueda string

//limitePorPagina limite de registros a mostrar en la pagina
var limitePorPagina = 10
var limitePorPaginaSat = 5

//NumPagina especifica el numero de página en la que se cargarán los registros
var NumPagina int

//IDElastic id obtenido de Elastic
var IDElastic bson.ObjectId
var arrIDMgo []bson.ObjectId
var arrIDElastic []bson.ObjectId
var arrToMongo []bson.ObjectId

var productoElasticSat UnidadModel.UnidadesSat
var arrProductoElasticSat []UnidadModel.UnidadesSat

var productoVistaSat UnidadModel.UnidadesSat
var arrProductoVistaSat []UnidadModel.UnidadesSat

//Numeros de Catálogos

var claveEstatusUnidad int = 165

//####################< INDEX (BUSQUEDA) >###########################

//IndexGet renderea al index de Unidad
func IndexGet(ctx *iris.Context) {
	var Send UnidadModel.SUnidad
	ctx.Render("Unidad/UnidadIndex.html", Send)
}

//CargaIndex regresa todos los datos necesarios para cargar el index de Unidad
func CargaIndex(ctx *iris.Context) {
	var Send UnidadModel.SUnidad

	Send.SIndex.STituloTabla = "Se han cargado todos(as) los(as) Unidades"
	Modelo := UnidadModel.MapaModelo()
	Send.SIndex.SNombresDeColumnas = Modelo["name"].([]string)
	Send.SIndex.SModeloDeColumnas = MoGeneral.GeneraModeloColumnas(Modelo)
	Unidades, err := UnidadModel.GetAll()
	Renglones := UnidadModel.GeneraRenglonesIndex(*Unidades)

	if Renglones != nil {
		Send.SIndex.SRenglones = Renglones
		if Renglones["rows"] != nil {
			Send.SEstado = true
			Send.SIndex.STituloTabla = "Se han cargado todos(as) los(as) Productos"
		} else {
			Send.SEstado = false
			Send.SMsj = "No se pudieron cargar los registros, ocurrió un problema al cargarlos de Mgo."
			Send.SIndex.STituloTabla = "No se pudieron cargar los registros, ocurrió un problema al cargarlos de Mgo."
		}
	} else {
		Send.SEstado = false
		Send.SMsj = "No se pudieron cargar los registros, ocurrió un problema al cargarlos de Mgo."
	}

	jData, err := json.Marshal(Send)
	if err != nil {
		fmt.Println(err)
	}
	ctx.Header().Set("Content-Type", "application/json")
	ctx.Write(jData)
	return

}

//CargaIndexEspecifico regresa los datos filtrados necesarios para cargar el index de Unidad
func CargaIndexEspecifico(ctx *iris.Context) {

	var Send UnidadModel.SUnidad

	// NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx)
	// Send.SSesion.Name = NameUsrLoged
	// Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	// Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	// if errSes != nil {
	// 	Send.SEstado = false
	// 	Send.SMsj = errSes.Error()
	// 	ctx.Render("ZError.html", Send)
	// 	return
	// }

	var TextoABuscar string
	TextoABuscar = ctx.FormValue("Texto")

	if TextoABuscar != "" {

		Send.SIndex.STituloTabla = "Se cargaron los(as) Unidades filtrados(as) por: " + TextoABuscar
		Modelo := UnidadModel.MapaModelo()
		Send.SIndex.SNombresDeColumnas = Modelo["name"].([]string)
		Send.SIndex.SModeloDeColumnas = MoGeneral.GeneraModeloColumnas(Modelo)

		docs, err := UnidadModel.BuscarEnElastic(TextoABuscar)
		if err != nil {
			fmt.Println(err)
			Send.SEstado = false
			Send.SMsj += " Ocurrió un error al consultar : " + err.Error()

			jData, err := json.Marshal(Send)
			if err != nil {
				fmt.Println(err)
			}
			ctx.Header().Set("Content-Type", "application/json")
			ctx.Write(jData)
			return
		}

		if docs.Hits.TotalHits > 0 {

			arrIDElastic = []bson.ObjectId{}
			for _, item := range docs.Hits.Hits {
				IDElastic = bson.ObjectIdHex(item.Id)
				arrIDElastic = append(arrIDElastic, IDElastic)
			}

		} else {

			Send.SEstado = false
			Send.SMsj += "No se encontraron resultados para: " + TextoABuscar

			jData, err := json.Marshal(Send)
			if err != nil {
				fmt.Println(err)
			}

			ctx.Header().Set("Content-Type", "application/json")
			ctx.Write(jData)
			return
		}

		fmt.Println("Se encontraron ", len(arrIDElastic), " registros.")

		Unidades, err := UnidadModel.GetEspecifics(arrIDElastic)
		Renglones := UnidadModel.GeneraRenglonesIndex(Unidades)

		if Renglones != nil {
			Send.SIndex.SRenglones = Renglones
			if Renglones["rows"] != nil {
				Send.SEstado = true
				Send.SIndex.STituloTabla = "Se han cargado todos(as) los(as) Productos"
			} else {
				Send.SEstado = false
				Send.SMsj = "No se pudieron cargar los registros, ocurrió un problema al cargarlos de Mgo."
				Send.SIndex.STituloTabla = "No se pudieron cargar los registros, ocurrió un problema al cargarlos de Mgo."
			}
		} else {
			Send.SEstado = false
			Send.SMsj = "No se pudieron cargar los registros, ocurrió un problema al cargarlos de Mgo."
		}

		jData, err := json.Marshal(Send)
		if err != nil {
			fmt.Println(err)
		}
		ctx.Header().Set("Content-Type", "application/json")
		ctx.Write(jData)
		return
	}

	jData, err := json.Marshal(Send)
	if err != nil {
		fmt.Println(err)
	}

	Send.SEstado = false
	Send.SMsj += " Se recibió una cadena de texto vacía, ingrese un texto a buscar e intente de nuevo."
	ctx.Header().Set("Content-Type", "application/json")
	ctx.Write(jData)
	return

}

//###########################< ALTA >################################

//AltaGet renderea al alta de Unidad
func AltaGet(ctx *iris.Context) {
	var Send UnidadModel.SUnidad
	Send.SGrupo = template.HTML(MoGeneral.CargaComboMostrarEnIndex(limitePorPaginaSat))
	ctx.Render("Unidad/UnidadAlta.html", Send)
}

//AltaPost regresa la petición post que se hizo desde el alta de Unidad
func AltaPost(ctx *iris.Context) {
	var Send UnidadModel.SUnidad
	var Unidad UnidadModel.UnidadMgo
	Unidad.ID = bson.NewObjectId()
	Send.Unidad.ID = Unidad.ID
	Unidad.FechaHora = time.Now()
	Unidad.Estatus = bson.ObjectIdHex("58e5677be75770120c60bee3")
	Send.Unidad.EEstatusUnidad.Ihtml = template.HTML(CargaCombos.CargaComboCatalogo(claveEstatusUnidad, "58e5677be75770120c60bee3"))
	Send.Unidad.EFechaHoraUnidad.FechaHora = Unidad.FechaHora
	var EstatusPeticion bool

	Nombre := ctx.FormValue("Nombre")
	Send.Unidad.ENombreUnidad.Nombre = Nombre
	Unidad.Nombre = Nombre
	if Nombre == "" {
		EstatusPeticion = true
		Send.Unidad.ENombreUnidad.IEstatus = true
		Send.Unidad.ENombreUnidad.IMsj = "El Nombre de la unidad es requerido."
	} else if err := Unidad.ConsultaExistenciaByFieldMgo("Nombre", Nombre); err != nil {
		EstatusPeticion = true
		Send.Unidad.ENombreUnidad.IEstatus = true
		Send.Unidad.ENombreUnidad.IMsj = "Ocurrió un error al verificar la unicidad del Nombre de la Unidad: " + err.Error()
	}

	Abreviatura := ctx.FormValue("Abreviatura")
	Send.Unidad.EAbreviaturaUnidad.Abreviatura = Abreviatura
	Unidad.Abreviatura = Abreviatura
	if Abreviatura == "" {
		EstatusPeticion = true
		Send.Unidad.EAbreviaturaUnidad.IEstatus = true
		Send.Unidad.EAbreviaturaUnidad.IMsj = "La Abreviatura de la unidad es requerida."
	} else if err := Unidad.ConsultaExistenciaByFieldMgo("Abreviatura", Abreviatura); err != nil {
		EstatusPeticion = true
		Send.Unidad.EAbreviaturaUnidad.IEstatus = true
		Send.Unidad.EAbreviaturaUnidad.IMsj = "Ocurrió un error al verificar la unicidad de la Abreviatura de la Unidad: " + err.Error()
	}

	CodigoSat := ctx.FormValue("CodigoSat")
	Send.Unidad.ECodigoSatUnidad.CodigoSat = CodigoSat
	Unidad.CodigoSat = CodigoSat
	if CodigoSat == "" {
		EstatusPeticion = true
		Send.Unidad.ECodigoSatUnidad.IEstatus = true
		Send.Unidad.ECodigoSatUnidad.IMsj = "El CódigoSat de la unidad es requerido."
	}
	// else if err := Unidad.ConsultaExistenciaByFieldMgo("CodigoSat", CodigoSat); err != nil {
	// 	EstatusPeticion = true
	// 	Send.Unidad.ECodigoSatUnidad.IEstatus = true
	// 	Send.Unidad.ECodigoSatUnidad.IMsj = "El Código del Sat de la unidad ya existe."
	// }

	if EstatusPeticion {
		Send.SEstado = false
		Send.SMsj = "Existen errores en su captura de la unidad, no se puede procesar su solicitud de Alta."
		ctx.Render("Unidad/UnidadAlta.html", Send)
		return
	}

	err := Unidad.InsertaMgo()
	if err != nil {
		Send.SEstado = false
		Send.SMsj = "Ocurrió un problema al dar de alta su Unidad en mongo, intente de nuevo más tarde."
		ctx.Render("Unidad/UnidadAlta.html", Send)
		return
	}

	err = Unidad.InsertaElastic()
	if err != nil {
		err1 := Unidad.EliminaByIDMgo()
		if err1 != nil {
			Send.SEstado = false
			Send.SMsj = "Ocurrió un problema al dar de alta su Unidad en elastic, el registro existe en Mongo: ." + err.Error()
			ctx.Render("Unidad/UnidadAlta.html", Send)
			return
		}

		Send.SEstado = false
		Send.SMsj = "Ocurrió un problema al dar de alta su Unidad en elastic, intente de nuevo más tarde, para evitar incongruencias el registro se dio de baja en Mongo."
		ctx.Render("Unidad/UnidadAlta.html", Send)
		return
	}

	Send.SEstado = true
	Send.SMsj = "La unidad ha sido dada de alta correctamente."
	//Send.Unidad = UnidadModel.Unidad{}
	ctx.Render("Unidad/UnidadDetalle.html", Send)
	return
}

//###########################< EDICION >###############################

//EditaGet renderea a la edición de Unidad
func EditaGet(ctx *iris.Context) {

	var Send UnidadModel.SUnidad
	id := ctx.Param("ID")
	if bson.IsObjectIdHex(id) {
		Unidad, err := UnidadModel.GetOne(bson.ObjectIdHex(id))
		if err != nil {
			Send.SEstado = false
			Send.SMsj = "Ocurrió un problema consultar el registro en la base de datos: " + err.Error()
			ctx.Render("Unidad/UnidadIndex.html", Send)
			return
		}

		if !MoGeneral.EstaVacio(Unidad) {
			Send.Unidad.ID = Unidad.ID
			Send.Unidad.ENombreUnidad.Nombre = Unidad.Nombre
			Send.Unidad.EAbreviaturaUnidad.Abreviatura = Unidad.Abreviatura
			Send.Unidad.ECodigoSatUnidad.CodigoSat = Unidad.CodigoSat
			Send.Unidad.EEstatusUnidad.Ihtml = template.HTML(CargaCombos.CargaComboCatalogo(claveEstatusUnidad, Unidad.Estatus.Hex()))
		} else {
			Send.SEstado = false
			Send.SMsj = "El documento no existe en la base de datos."
			ctx.Render("Unidad/UnidadIndex.html", Send)
			return
		}

	} else {
		Send.SEstado = false
		Send.SMsj = "No se recibió un parámetro de búsqueda adecuado."
		ctx.Render("Unidad/UnidadIndex.html", Send)
		return
	}

	Send.SEstado = true
	Send.SGrupo = template.HTML(MoGeneral.CargaComboMostrarEnIndex(limitePorPaginaSat))
	ctx.Render("Unidad/UnidadEdita.html", Send)

}

//EditaPost regresa el resultado de la petición post generada desde la edición de Unidad
func EditaPost(ctx *iris.Context) {

	var Send UnidadModel.SUnidad
	var Unidad UnidadModel.UnidadMgo
	var EstatusPeticion bool
	msj := ``

	ID := ctx.FormValue("ID")
	if bson.IsObjectIdHex(ID) {
		var err error
		Unidad, err = UnidadModel.GetOne(bson.ObjectIdHex(ID))
		if err != nil {
			EstatusPeticion = true
			msj += "  ," + err.Error()
		}
		if MoGeneral.EstaVacio(Unidad) {
			EstatusPeticion = true
			msj += " ,El documento que intenta editar no hace referencia a uno existente."
		}
	}

	Nombre := ctx.FormValue("Nombre")
	Send.Unidad.ENombreUnidad.Nombre = Nombre
	if Nombre == "" {
		EstatusPeticion = true
		Send.Unidad.ENombreUnidad.IEstatus = true
		Send.Unidad.ENombreUnidad.IMsj = "El Nombre de la unidad es requerido."
	} else if Unidad.Nombre != Nombre {
		if err := Unidad.ConsultaExistenciaByFieldMgo("Nombre", Nombre); err != nil {
			EstatusPeticion = true
			Send.Unidad.ENombreUnidad.IEstatus = true
			Send.Unidad.ENombreUnidad.IMsj = "El Nombre de la unidad ya existe."
		}
	}

	Unidad.Nombre = Nombre

	Abreviatura := ctx.FormValue("Abreviatura")
	Send.Unidad.EAbreviaturaUnidad.Abreviatura = Abreviatura
	if Abreviatura == "" {
		EstatusPeticion = true
		Send.Unidad.EAbreviaturaUnidad.IEstatus = true
		Send.Unidad.EAbreviaturaUnidad.IMsj = "La Abreviatura de la unidad es requerida."
	} else if Unidad.Abreviatura != Abreviatura {
		if err := Unidad.ConsultaExistenciaByFieldMgo("Abreviatura", Abreviatura); err != nil {
			EstatusPeticion = true
			Send.Unidad.EAbreviaturaUnidad.IEstatus = true
			Send.Unidad.EAbreviaturaUnidad.IMsj = "La Abreviatura de la unidad ya existe."
		}
	}
	Unidad.Abreviatura = Abreviatura

	CodigoSat := ctx.FormValue("CodigoSat")
	Send.Unidad.ECodigoSatUnidad.CodigoSat = CodigoSat
	Unidad.CodigoSat = CodigoSat
	if CodigoSat == "" {
		EstatusPeticion = true
		Send.Unidad.ECodigoSatUnidad.IEstatus = true
		Send.Unidad.ECodigoSatUnidad.IMsj = "El CódigoSat de la unidad es requerido."
	}

	Estatus := ctx.FormValue("Estatus")
	Send.Unidad.EEstatusUnidad.Ihtml = template.HTML(CargaCombos.CargaComboCatalogo(claveEstatusUnidad, Estatus))

	if bson.IsObjectIdHex(Estatus) {
		Unidad.Estatus = bson.ObjectIdHex(Estatus)
	} else {
		EstatusPeticion = true
		Send.Unidad.EEstatusUnidad.IEstatus = true
		Send.Unidad.EEstatusUnidad.IMsj = "El Estatus es obligatorio o no se recibió una referencia adecuada."
	}

	if EstatusPeticion {
		Send.SEstado = false
		Send.SMsj = msj + "Existen errores en su captura de la unidad, no se puede procesar su solicitud de Alta."
		ctx.Render("Unidad/UnidadEdita.html", Send)
		return
	}

	err := Unidad.ReemplazaMgo()
	if err != nil {
		Send.SEstado = false
		Send.SMsj = "Ocurrió un problema al actualizar su Unidad en mongo, intente de nuevo más tarde."
		ctx.Render("Unidad/UnidadEdita.html", Send)
		return
	}

	err = Unidad.ActualizaElastic()
	if err != nil {
		Send.SEstado = false
		Send.SMsj = "Ocurrió un problema al actualizar su Unidad en elastic, intente de nuevo más tarde."
		ctx.Render("Unidad/UnidadEdita.html", Send)
		return
	}

	Send.SEstado = true
	Send.SMsj = "La unidad ha sido actualizada correctamente."
	ctx.Render("Unidad/UnidadDetalle.html", Send)
	return
}

//#################< DETALLE >####################################

//DetalleGet renderea al index.html
func DetalleGet(ctx *iris.Context) {

	var Send UnidadModel.SUnidad
	id := ctx.Param("ID")
	if bson.IsObjectIdHex(id) {
		Unidad, err := UnidadModel.GetOne(bson.ObjectIdHex(id))
		if err != nil {
			Send.SEstado = false
			Send.SMsj = "Ocurrió un problema consultar el registro en la base de datos: " + err.Error()
			ctx.Render("Unidad/UnidadIndex.html", Send)
			return
		}

		if !MoGeneral.EstaVacio(Unidad) {
			Send.Unidad.ID = Unidad.ID
			Send.Unidad.ENombreUnidad.Nombre = Unidad.Nombre
			Send.Unidad.EAbreviaturaUnidad.Abreviatura = Unidad.Abreviatura
			Send.Unidad.ECodigoSatUnidad.CodigoSat = Unidad.CodigoSat
			Send.Unidad.EEstatusUnidad.Ihtml = template.HTML(CargaCombos.CargaComboCatalogo(claveEstatusUnidad, Unidad.Estatus.Hex()))
			Send.Unidad.EFechaHoraUnidad.FechaHora = Unidad.FechaHora
		} else {
			Send.SEstado = false
			Send.SMsj = "El documento no existe en la base de datos."
			ctx.Render("Unidad/UnidadIndex.html", Send)
			return
		}

	} else {
		Send.SEstado = false
		Send.SMsj = "No se recibió un parámetro de búsqueda adecuado."
		ctx.Render("Unidad/UnidadIndex.html", Send)
		return
	}

	Send.SEstado = true
	ctx.Render("Unidad/UnidadDetalle.html", Send)
	return
}

//DetallePost renderea al index.html
func DetallePost(ctx *iris.Context) {
	var Send UnidadModel.SUnidad

	// NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx) //Retorna los datos de la session
	// Send.SSesion.Name = NameUsrLoged
	// Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	// Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	// if errSes != nil {
	// 	Send.SEstado = false
	// 	Send.SMsj = errSes.Error()
	// 	ctx.Render("ZError.html", Send)
	// 	return
	// }

	//###### TU CÓDIGO AQUÍ PROGRAMADOR
	//#####

	ctx.Render("Unidad/UnidadDetalle.html", Send)
}

//####################< RUTINAS ADICIONALES >##########################

//MuestraIndexPorGrupoS regresa template de busqueda y paginacion de acuerdo a la agrupacion solicitada
func MuestraIndexPorGrupoS(ctx *iris.Context) {

	var Send UnidadModel.ListaVista

	grupo := ctx.FormValue("GrupoSat")
	if grupo != "" {
		gru, _ := strconv.Atoi(grupo)
		limitePorPaginaSat = gru
	}

	cadenaBusqueda = ctx.FormValue("Cadena")

	index := "unidadessatfull"
	tipo := "unidadessatfull"

	if cadenaBusqueda != "" {
		var arrProductoElasticSat2 *[]UnidadModel.UnidadesSat
		var err error
		arrProductoElasticSat2, err = UnidadModel.BuscarEnElasticSatDefault(index, tipo, cadenaBusqueda)
		if err != nil {
			Send.SEstado = false
			Send.SMsj = err.Error()
			jData, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.Write(jData)
			return
		}

		numeroRegistrosSat = len(*arrProductoElasticSat2)

		if numeroRegistrosSat == 0 {
			Send.SEstado = false
			Send.SMsj = "No se encontró ningún registro para mostrar."
			jData, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.Write(jData)
			return
		}
		arrProductoElasticSat = *arrProductoElasticSat2

		arrProductoVistaSat = []UnidadModel.UnidadesSat{}
		if numeroRegistrosSat <= limitePorPaginaSat {
			for _, v := range arrProductoElasticSat[0:numeroRegistrosSat] {
				arrProductoVistaSat = append(arrProductoVistaSat, v)
			}
		} else if numeroRegistrosSat >= limitePorPaginaSat {
			for _, v := range arrProductoElasticSat[0:limitePorPaginaSat] {
				arrProductoVistaSat = append(arrProductoVistaSat, v)
			}
		}

		Cabecera, Cuerpo := UnidadModel.GeneraTemplatesBusquedaSat(arrProductoVistaSat)
		Send.SCabecera = template.HTML(Cabecera)
		Send.SBody = template.HTML(Cuerpo)

		paginasTotalesSat = MoGeneral.Totalpaginas(numeroRegistrosSat, limitePorPaginaSat)
		Paginacion := MoGeneral.ConstruirPaginacion2(paginasTotalesSat, 1)
		Send.SPaginacion = template.HTML(Paginacion)

		Send.SGrupo = template.HTML(MoGeneral.CargaComboMostrarEnIndex(limitePorPaginaSat))

	} else {
		if len(arrProductoElasticSat) > 0 {

			arrProductoVistaSat = []UnidadModel.UnidadesSat{}
			if numeroRegistrosSat <= limitePorPaginaSat {
				for _, v := range arrProductoElasticSat[0:numeroRegistrosSat] {
					arrProductoVistaSat = append(arrProductoVistaSat, v)
				}
			} else if numeroRegistrosSat >= limitePorPaginaSat {
				for _, v := range arrProductoElasticSat[0:limitePorPaginaSat] {
					arrProductoVistaSat = append(arrProductoVistaSat, v)
				}
			}

			Cabecera, Cuerpo := UnidadModel.GeneraTemplatesBusquedaSat(arrProductoVistaSat)
			Send.SCabecera = template.HTML(Cabecera)
			Send.SBody = template.HTML(Cuerpo)

			paginasTotalesSat = MoGeneral.Totalpaginas(numeroRegistrosSat, limitePorPaginaSat)
			Paginacion := MoGeneral.ConstruirPaginacion2(paginasTotalesSat, 1)
			Send.SPaginacion = template.HTML(Paginacion)

			Send.SGrupo = template.HTML(MoGeneral.CargaComboMostrarEnIndex(limitePorPaginaSat))
		}

	}

	Send.SEstado = true
	jData, _ := json.Marshal(Send)
	ctx.Header().Set("Content-Type", "application/json")
	ctx.Write(jData)
	return
}

//BuscaPaginaSat regresa la tabla de busqueda y su paginacion en el momento de especificar página
func BuscaPaginaSat(ctx *iris.Context) {

	var Send UnidadModel.ListaVista
	Pagina := ctx.FormValue("Pag")

	if Pagina != "" {
		num, _ := strconv.Atoi(Pagina)
		if num > 0 {
			if numeroRegistrosSat > 0 {
				NumPagina = num
				skip := limitePorPaginaSat * (NumPagina - 1)
				limite := skip + limitePorPaginaSat

				arrProductoVistaSat = []UnidadModel.UnidadesSat{}
				if NumPagina == paginasTotalesSat {
					final := int(numeroRegistrosSat) % limitePorPaginaSat
					if final == 0 {
						for _, v := range arrProductoElasticSat[skip:limite] {
							arrProductoVistaSat = append(arrProductoVistaSat, v)
						}
					} else {
						for _, v := range arrProductoElasticSat[skip : skip+final] {
							arrProductoVistaSat = append(arrProductoVistaSat, v)
						}
					}

				} else {
					for _, v := range arrProductoElasticSat[skip:limite] {
						arrProductoVistaSat = append(arrProductoVistaSat, v)
					}
				}

				Cabecera, Cuerpo := UnidadModel.GeneraTemplatesBusquedaSat(arrProductoVistaSat)
				Send.SCabecera = template.HTML(Cabecera)
				Send.SBody = template.HTML(Cuerpo)

				paginasTotalesSat = MoGeneral.Totalpaginas(numeroRegistrosSat, limitePorPaginaSat)
				Paginacion := MoGeneral.ConstruirPaginacion2(paginasTotalesSat, num)
				Send.SPaginacion = template.HTML(Paginacion)

				Send.SEstado = true
			} else {
				Send.SMsj = "No se pueden mostrar páginas, no existen registros que mostrar."
				Send.SEstado = false
			}
		} else {
			Send.SMsj = "No hay páginas Cero."
			Send.SEstado = false
		}

	} else {
		Send.SMsj = "No se recibió una cadena de consulta, favor de escribirla."
		Send.SEstado = false
	}

	jData, _ := json.Marshal(Send)
	ctx.Header().Set("Content-Type", "application/json")
	ctx.Write(jData)
	return
}

//ConsultaElasticSat recupera la cedena de texto a buscar y regresa los datos con la paginación
func ConsultaElasticSat(ctx *iris.Context) {

	var Send UnidadModel.ListaVista

	grupo := ctx.FormValue("GrupoSat")
	if grupo != "" {
		gru, _ := strconv.Atoi(grupo)
		limitePorPaginaSat = gru
	}

	cadena := ctx.FormValue("Cadena")

	index := "unidadessatfull"
	tipo := "unidadessatfull"

	if cadena != "" {
		var arrProductoElasticSat2 *[]UnidadModel.UnidadesSat
		var err error
		arrProductoElasticSat2, err = UnidadModel.BuscarEnElasticSatDefault(index, tipo, cadena)
		if err != nil {
			Send.SEstado = false
			Send.SMsj = err.Error()
			jData, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.Write(jData)
			return
		}
		numeroRegistrosSat = len(*arrProductoElasticSat2)

		if numeroRegistrosSat == 0 {
			Send.SEstado = false
			Send.SMsj = "No se encontró ningún registro para mostrar."
			jData, _ := json.Marshal(Send)
			ctx.Header().Set("Content-Type", "application/json")
			ctx.Write(jData)
			return
		}

		arrProductoElasticSat = *arrProductoElasticSat2

		arrProductoVistaSat = []UnidadModel.UnidadesSat{}
		if numeroRegistrosSat <= limitePorPaginaSat {
			for _, v := range arrProductoElasticSat[0:numeroRegistrosSat] {
				arrProductoVistaSat = append(arrProductoVistaSat, v)
			}
		} else if numeroRegistrosSat >= limitePorPaginaSat {
			for _, v := range arrProductoElasticSat[0:limitePorPaginaSat] {
				arrProductoVistaSat = append(arrProductoVistaSat, v)
			}
		}

		Cabecera, Cuerpo := UnidadModel.GeneraTemplatesBusquedaSat(arrProductoVistaSat)
		Send.SCabecera = template.HTML(Cabecera)
		Send.SBody = template.HTML(Cuerpo)

		paginasTotalesSat = MoGeneral.Totalpaginas(numeroRegistrosSat, limitePorPaginaSat)
		Paginacion := MoGeneral.ConstruirPaginacion2(paginasTotalesSat, 1)
		Send.SPaginacion = template.HTML(Paginacion)

		Send.SEstado = true
	} else {
		Send.SEstado = false
		Send.SMsj = "No se recibió una cadena de consulta, favor de escribirla."
	}

	jData, _ := json.Marshal(Send)
	ctx.Header().Set("Content-Type", "application/json")
	ctx.Write(jData)
	return
}
