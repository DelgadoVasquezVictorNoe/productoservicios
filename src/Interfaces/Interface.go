package Interfaces

import (
	"gopkg.in/mgo.v2/bson"
)

//IOperaciones interface con métodos de tipo crud para Módulos
type IOperaciones interface {
	InsertaMgo() error
	InsertaElastic() error

	ActualizaMgo(campos []string, valores []interface{}) error
	ActualizaElastic(campos []string, valores []interface{}) error

	ReemplazaMgo() error
	ReemplazaElastic() error

	EliminaByIDMgo() error
	EliminaByIDElastic() error
}

//IConsultas interface con métodos de consultas básicas para Conexiones (Elastic-Mongo) *Pensándose
type IConsultas interface {
	GetOne(ID bson.ObjectId) (interface{}, error)
	GetAll() ([]interface{}, error)
	GetOneById(ID bson.ObjectId) (interface{}, error)
	GetOneByFields(campos []string, valores []interface{}) (interface{}, error)
	GetManyByFields(campos []string, valores []interface{}) ([]interface{}, error)
}

//Ejemplo de uso de una función Polimorfa, al invocar esta función el argumento Documento
//puede ser cualquier estructura con métodos propios llamados igual
//que los que se han declarado en la Interfaz. Este es un polimorfismo de funciones CRUD

func InsertaDocumento(Documento IOperaciones) error {
	if err := Documento.InsertaMgo(); err != nil {
		return err
	}
	return nil
}
