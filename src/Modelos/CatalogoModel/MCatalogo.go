package CatalogoModel

import (
	"fmt"
	"strings"

	"gopkg.in/mgo.v2/bson"

	"../../Modulos/Conexiones"
	"../../Modulos/Variables"
)

//GetAll Regresa todos los documentos existentes de Mongo (Por Coleccion)
func GetAll() (*[]CatalogoMgo, error) {
	var rex []CatalogoMgo

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionCatalogo)
	if err != nil {
		fmt.Println(err)
	}

	err = c.Find(nil).All(&rex)
	if err != nil {
		fmt.Println(err)
	}

	return &rex, nil
}

func GetOne(ID bson.ObjectId) (*CatalogoMgo, error) {
	var rex CatalogoMgo

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionCatalogo)
	if err != nil {
		fmt.Println(err)
	}

	err = c.Find(bson.M{"_id": ID}).One(&rex)
	if err != nil {
		fmt.Println(err)
	}

	return &rex, nil
}

//GetEspecificByFields regresa un conjunto de documentos específicos de Mongo (Por Coleccion)
func GetEspecificByFields(field string, valor interface{}) (CatalogoMgo, error) {
	var result CatalogoMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionCatalogo)
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	err = c.Find(bson.M{field: valor}).One(&result)
	if err != nil {
		fmt.Println(err)
		return result, err
	}

	return result, nil
}

//GetSubEspecificByFields regresa un documento de Mongo especificando un campo y un determinado valor
func GetSubEspecificByFields(field string, valor interface{}) (ValoresMgo, error) {
	var resultx CatalogoMgo
	var result ValoresMgo

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	err = c.Find(bson.M{field: valor}).Select(bson.M{"Valores.$": 1, "_id": 0}).One(&resultx)
	if err != nil {
		fmt.Println(err)
		return result, err
	}

	if len(resultx.Valores) > 0 {
		return resultx.Valores[0], nil
	}
	return result, nil
}

//RegresaIDEstatusActivo regresa el Id del catálogo de estatus de catálogos donde haya un activo.
//debe especificarse la clave del catálogo.
func RegresaIDEstatusActivo(Clave int) bson.ObjectId {
	var result bson.ObjectId
	Catalogo, _ := GetEspecificByFields("Clave", int64(Clave))
	for _, v := range Catalogo.Valores {
		if strings.ToUpper(v.Valor) == "ACTIVO" {
			result = v.ID
		}
	}
	return result
}
