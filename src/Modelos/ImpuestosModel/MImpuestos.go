package ImpuestosModel

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

//CargaComboImpuestosMulti Carga el combo para un multi select
func CargaComboImpuestosMulti(Clave string) string {

	Impuestos, err := ConsultaWSGet2("http://192.168.1.89:8090/CatalogoSatApi/Impuestos", time.Duration(30))
	if err != nil {
		fmt.Println(err)
		return `<option value="" selected>ERROR AL CARGAR IMPUESTOS</option>`
	}

	templ := ``

	for _, v := range Impuestos {
		if Clave == v.Clave {
			templ += `<option value="` + v.Clave + `" selected>` + v.Descripcion + `</option>`
		} else {
			templ += `<option value="` + v.Clave + `">` + v.Descripcion + `</option>`
		}
	}
	return templ
}

//ConsultaWSGet2 consulta un web service con un método Get, (la URL debe tener ya concatenadas las variables y sus valores.)
//responde con un arreglo de bytes listo para convertise a un tipo de documento de comunicación.
func ConsultaWSGet2(URL string, Timeout time.Duration) ([]ImpuestosMgo, error) {
	var Send []ImpuestosMgo

	var netClient = &http.Client{
		Timeout: time.Second * Timeout,
	}

	response, err := netClient.Get(URL)
	if err != nil {
		fmt.Println(err)
		return Send, err
	}
	defer response.Body.Close()

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
		return Send, err
	}

	err = json.Unmarshal(responseData, &Send)
	if err != nil {
		fmt.Println(err)
		return Send, err
	}

	return Send, nil
}
