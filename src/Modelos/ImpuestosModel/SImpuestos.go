package ImpuestosModel

//ImpuestosMgo estructura para manejar impuestos
type ImpuestosMgo struct {
	Clave         string `json:"Clave"`
	Descripcion   string `json:"Descripcion"`
	Retencion     string `json:"Retencion"`
	Traslado      string `json:"Traslado"`
	LocalOFederal string `json:"LocalOFederal"`
	EntidadAplica string `json:"EntidadAplica"`
}
