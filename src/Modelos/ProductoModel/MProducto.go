package ProductoModel

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"../../Modulos/Archivos"
	"../../Modulos/CargaCombos"
	"../../Modulos/Conexiones"
	"../../Modulos/General"
	"../../Modulos/Variables"

	"gopkg.in/mgo.v2/bson"
	"gopkg.in/olivere/elastic.v5"
)

//#########################< FUNCIONES GENERALES MGO >###############################

//GetAll Regresa todos los documentos existentes de Mongo (Por Coleccion)
func GetAll() ([]ProductoMgo, error) {
	var result []ProductoMgo

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	err = c.Find(nil).All(&result)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return result, nil
}

//CountAll Regresa todos los documentos existentes de Mongo (Por Coleccion)
func CountAll() (int, error) {
	var result int
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return 0, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	result, err = c.Find(nil).Count()
	if err != nil {
		fmt.Println(err)
		return 0, err
	}

	return result, nil
}

//GetOne Regresa un documento específico de Mongo (Por Coleccion)
func GetOne(ID bson.ObjectId) (ProductoMgo, error) {
	var result ProductoMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	err = c.Find(bson.M{"_id": ID}).One(&result)
	if err != nil {
		fmt.Println(err)
		return result, err
	}

	return result, nil
}

//GetEspecifics rsegresa un conjunto de documentos específicos de Mongo (Por Coleccion)
func GetEspecifics(Ides []bson.ObjectId) ([]ProductoMgo, error) {
	var result []ProductoMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	In := bson.M{"$in": Ides}
	err = c.Find(bson.M{"_id": In}).All(&result)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return result, nil
}

//GetEspecificByFields regresa un documento de Mongo especificando un campo y un determinado valor
func GetEspecificByFields(field string, valor interface{}) (ProductoMgo, error) {
	var result ProductoMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	err = c.Find(bson.M{field: valor}).One(&result)
	if err != nil {
		//fmt.Println("El error en la base de datos es: ", err)
		fmt.Println(err)
		//return result, err
	}

	return result, nil
}

//GetIDByField regresa un documento específico de Mongo (Por Coleccion)
func GetIDByField(field string, valor interface{}) (bson.ObjectId, error) {
	var result ProductoMgo
	var ID bson.ObjectId
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return ID, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	err = c.Find(bson.M{field: valor}).One(&result)
	if err != nil {
		fmt.Println(err)
		return ID, err
	}

	return result.ID, nil
}

//MapaModelo regresa el mapa del modelo para jqgrid
func MapaModelo() map[string]interface{} {
	var ModelOptions map[string]interface{}
	ModelOptions = make(map[string]interface{})
	ModelOptions["name"] = []string{"id", "Codigo", "Nombre", "Tipo", "Unidad", "CodigoSat", "Compra Decimal", "Venta Decimal", "Etiquetas", "Estatus", "FechaHora"}
	ModelOptions["index"] = []string{"id", "Codigo", "Nombre", "Tipo", "Unidad", "CodigoSat", "VentaFraccionCompra", "VentaFraccionVenta", "Etiquetas", "Estatus", "FechaHora"}
	ModelOptions["align"] = []string{"left", "left", "left", "left", "left", "left", "left", "left", "left", "left", "left", "left"}
	ModelOptions["editable"] = []bool{false, false, false, false, false, false, false, false, false, false, false}
	ModelOptions["hidden"] = []bool{true, false, false, false, false, false, false, false, false, false, false}
	ModelOptions["search"] = []bool{true, true, true, true, true, true, true, true, true, true, true}
	ModelOptions["stype"] = []string{"text", "text", "text", "select", "text", "text", "text", "text", "text", "select", "text"}
	ModelOptions["sorteable"] = []bool{true, true, true, true, true, true, true, true, true, true, true}
	ModelOptions["sorttype"] = []string{"text", "text", "text", "text", "text", "text", "text", "text", "text", "text", "date"}

	var Textos map[string]interface{}
	Textos = make(map[string]interface{})
	Textos["sopt"] = []string{"eq", "ne", "cn"}

	var Numeros map[string]interface{}
	Numeros = make(map[string]interface{})
	Numeros["sopt"] = []string{"eq", "ne", "lt", "le", "gt", "ge"}

	var Fechas map[string]interface{}
	Fechas = make(map[string]interface{})
	Fechas["sopt"] = []string{"eq", "ne", "lt", "le", "gt", "ge"}
	Fechas["dataInit"] = `datePick`

	var Formatos map[string]interface{}
	Formatos = make(map[string]interface{})
	Formatos["srcformat"] = "d m y"
	Formatos["newformat"] = "d m y"

	Fechas["formatoptions"] = Formatos

	var Atributos map[string]interface{}
	Atributos = make(map[string]interface{})
	Atributos["title"] = "Seleccionar Fecha"

	Fechas["attr"] = Atributos

	var ComboTipo map[string]interface{}
	ComboTipo = make(map[string]interface{})
	ComboTipo["sopt"] = []string{"eq", "ne", "cn"}
	ComboTipo["value"] = CargaCombos.CargaComboCatalogoGrid(162)

	var ComboEstatus map[string]interface{}
	ComboEstatus = make(map[string]interface{})
	ComboEstatus["sopt"] = []string{"eq", "ne", "cn"}
	ComboEstatus["value"] = CargaCombos.CargaComboCatalogoGrid(161)

	ModelOptions["searchoptions"] = []interface{}{Textos, Textos, Textos, ComboTipo, Textos, Textos, Textos, Textos, Textos, ComboEstatus, Fechas}

	return ModelOptions
}

//MapaModeloBusqueda regresa el mapa del modelo para jqgrid
func MapaModeloBusqueda() map[string]interface{} {
	var ModelOptions map[string]interface{}
	ModelOptions = make(map[string]interface{})
	ModelOptions["name"] = []string{"id", "Codigos", "Nombre", "Tipo", "CodigoSat", "Venta Decimal?", "Etiquetas", "Estatus", "Unidad", "UnidadVentaId"}
	ModelOptions["index"] = []string{"id", "Codigos", "Nombre", "Tipo", "CodigoSat", "VentaFraccionVenta", "Etiquetas", "Estatus", "UnidadVenta", "UnidadVentaId"}
	ModelOptions["align"] = []string{"left", "left", "left", "left", "left", "left", "left", "left", "left", "left"}
	ModelOptions["editable"] = []bool{false, false, false, false, false, false, false, false, false, false}
	ModelOptions["hidden"] = []bool{true, false, false, false, false, false, false, false, true, true}
	ModelOptions["search"] = []bool{true, true, true, true, true, true, true, true, false, false}
	ModelOptions["stype"] = []string{"text", "text", "text", "select", "text", "text", "text", "select", "text", "text"}
	ModelOptions["sorteable"] = []bool{true, true, true, true, true, true, true, true, false, false}
	ModelOptions["sorttype"] = []string{"text", "text", "text", "text", "text", "text", "text", "text", "text", "text"}

	var Textos map[string]interface{}
	Textos = make(map[string]interface{})
	Textos["sopt"] = []string{"eq", "ne", "cn"}

	var Numeros map[string]interface{}
	Numeros = make(map[string]interface{})
	Numeros["sopt"] = []string{"eq", "ne", "lt", "le", "gt", "ge"}

	var ComboTipo map[string]interface{}
	ComboTipo = make(map[string]interface{})
	ComboTipo["sopt"] = []string{"eq", "ne", "cn"}
	ComboTipo["value"] = CargaCombos.CargaComboCatalogoGrid(162)

	var ComboEstatus map[string]interface{}
	ComboEstatus = make(map[string]interface{})
	ComboEstatus["sopt"] = []string{"eq", "ne", "cn"}
	ComboEstatus["value"] = CargaCombos.CargaComboCatalogoGrid(161)

	ModelOptions["searchoptions"] = []interface{}{Textos, Textos, Textos, ComboTipo, Textos, Textos, Textos, ComboEstatus, Textos, Textos}

	return ModelOptions
}

//MapaModeloBusquedaProveedor regresa el mapa del modelo para jqgrid
//WebService - Proveedores
func MapaModeloBusquedaProveedor() map[string]interface{} {
	var ModelOptions map[string]interface{}
	ModelOptions = make(map[string]interface{})
	ModelOptions["name"] = []string{"RFC", "CORREO", "TELEFONO"}
	ModelOptions["index"] = []string{"RFC", "CORREO", "TELEFONO"}
	ModelOptions["align"] = []string{"left", "left", "left"}
	ModelOptions["editable"] = []bool{false, false, false}
	ModelOptions["hidden"] = []bool{false, false, false}
	ModelOptions["search"] = []bool{true, true, true}
	ModelOptions["stype"] = []string{"text", "text", "text"}
	ModelOptions["sorteable"] = []bool{true, true, true}
	ModelOptions["sorttype"] = []string{"text", "text", "text"}

	var Textos map[string]interface{}
	Textos = make(map[string]interface{})
	Textos["sopt"] = []string{"eq", "ne", "cn"}

	var Numeros map[string]interface{}
	Numeros = make(map[string]interface{})
	Numeros["sopt"] = []string{"eq", "ne", "lt", "le", "gt", "ge"}

	var ComboTipo map[string]interface{}
	ComboTipo = make(map[string]interface{})
	ComboTipo["sopt"] = []string{"eq", "ne", "cn"}
	ComboTipo["value"] = CargaCombos.CargaComboCatalogoGrid(161)

	ModelOptions["searchoptions"] = []interface{}{Textos, Textos, Textos}

	return ModelOptions
}

//GeneraRenglonesIndex crea un mapa para crear el json de renglones del index
func GeneraRenglonesIndex(Productos []ProductoMgo) map[string]interface{} {
	var Mapas []map[string]interface{}

	for _, v := range Productos {
		var Mapa map[string]interface{}
		Mapa = make(map[string]interface{})

		Mapa["Nombre"] = v.Nombre
		Mapa["Tipo"] = v.Tipo.Hex()
		//Mapa["UnidadVenta"] = v.UnidadVenta.Default.Hex()
		//Mapa["UnidadCompra"] = v.UnidadCompra.Default.Hex()
		Mapa["Unidad"] = v.Unidad.Default.Hex()
		Mapa["CodigoSat"] = v.CodigoSat
		Mapa["VentaFraccionCompra"] = v.VentaFraccionCompra
		Mapa["VentaFraccionVenta"] = v.VentaFraccionVenta
		Mapa["Etiquetas"] = v.Etiquetas
		Mapa["Estatus"] = v.Estatus.Hex()
		Mapa["FechaHora"] = v.FechaHora.Format("2006/1/2")
		Mapas = append(Mapas, Mapa)

	}

	var Mapita map[string]interface{}
	Mapita = make(map[string]interface{})
	Mapita["rows"] = Mapas

	return Mapita
}

// GeneraTemplateBusquedaDeProductos Genera el template de Busqueda de los productos encontrados por elastic
func GeneraTemplateBusquedaDeProductos(Productos []ProductoMgo) string {

	busqueda := ``
	aux := ``

	for _, v := range Productos {

		busqueda += `<tr id="` + v.ID.Hex() + `">`

		htmlImagenes, _, _ := Archivos.GetStrImg(v.Imagenes)

		// if src == "" {
		// 	htmlimg, _, _ := Archivos.GetStrImg([]bson.ObjectId{bson.ObjectIdHex("59df8b51e757702a84b21d9d")})
		// 	busqueda += `<td>` + htmlimg + `</td>`
		// } else {
		busqueda += `<td>` + htmlImagenes + `</td>`
		//}

		aux = ``
		for _, valores := range v.Codigos.Valores {
			aux += valores + `,`
		}

		busqueda += `<td>` + aux[:len(aux)-1] + `</td>`
		busqueda += `<td>` + v.Nombre + `</td>`

		// unidades := CargaCombos.CargaComboUnidadesEspecifico(v.UnidadVenta.Default, v.UnidadVenta.Valores)

		// //unidad, _ := UnidadModel.GetSubEspecificByFields("Datos._id", v.UnidadVenta.Default)

		// busqueda += `<td><select  class="form-control selectpicker">` + unidades + `</select></td>`

		// if v.VentaFraccionVenta {
		// 	busqueda += `<td id="thisone"><input name="Cantidad" id = "` + v.ID.Hex() + `"  fraccion="` + strconv.FormatBool(v.VentaFraccionVenta) + `"  type="number" class="form-control" value=""></td>`
		// } else {
		// 	busqueda += `<td id="thisone"><input name="Cantidad" id = "` + v.ID.Hex() + `"  fraccion="` + strconv.FormatBool(v.VentaFraccionVenta) + `"  type="number" class="form-control" value=""></td>`
		// }

		busqueda += `<td><input type="checkbox" name="ModalSeleccione" value="` + v.ID.Hex() + `"></td>`

		// busqueda += `
		// 	<td>
		// 	<button type="button" class="btn btn-primary" onclick="SeleccionarProducto(this);">
		// 		<span class="glyphicon glyphicon-ok"></span>
		// 	</button>
		// 	</td>`

		busqueda += `</tr>`
		busqueda += `<script>`
		busqueda += `$("input[name='Cantidad']").keydown(function(e) {
									if (e.which == 13 || e.keyCode == 13) {
										e.preventDefault();
										SeleccionarProducto(this);
									}
								});`
		busqueda += `</script>`
	}

	return busqueda
}

// GeneraTemplateProducto Genera el template de Busqueda de los productos encontrados por elastic
func GeneraTemplateProducto(v ProductoMgo) string {

	busqueda := ``
	aux := ``

	busqueda += `<tr id="` + v.ID.Hex() + `">`
	aux = ``
	for _, valores := range v.Codigos.Valores {
		aux += valores + `,`
	}

	busqueda += `<td width="10%"><input type="text" class="form-control" name="CodigosKit" value="` + aux[:len(aux)-1] + `" readonly></td>`
	busqueda += `<td width="60%"><input type="text" class="form-control" name="DescripcionesKit" value="` + v.Nombre + `" readonly></td>`

	unidades := CargaCombos.CargaComboUnidadesEspecifico(v.Unidad.Default)

	//unidad, _ := UnidadModel.GetSubEspecificByFields("Datos._id", v.UnidadVenta.Default)

	busqueda += `<td><select  class="form-control selectpicker">` + unidades + `</select></td>`

	if v.VentaFraccionVenta {
		busqueda += `<td id="thisone"><input type="hidden" name="IdeKit" value="` + v.ID.Hex() + `"><input name="CantidadKit" id = "` + v.ID.Hex() + `"  fraccion="` + strconv.FormatBool(v.VentaFraccionVenta) + `"  type="number" class="form-control" value="1"></td>`
	} else {
		busqueda += `<td id="thisone"><input type="hidden" name="IdeKit" value="` + v.ID.Hex() + `"><input name="CantidadKit" id = "` + v.ID.Hex() + `"  fraccion="` + strconv.FormatBool(v.VentaFraccionVenta) + `"  type="number" class="form-control" value="1"></td>`
	}
	// busqueda += `<td><input type="radio" name="ModalSeleccione" value="` + v.ID.Hex() + `"></td>`
	busqueda += `<td><button type="button" class="btn btn-danger deleteButton"><span class="glyphicon glyphicon-trash btn-xs"></span></button></td>`

	busqueda += `</tr>`
	busqueda += `<script>`
	busqueda += `$("input[name='Cantidad']").keydown(function(e) {
										if (e.which == 13 || e.keyCode == 13) {
											e.preventDefault();
											SeleccionarProducto(this);
										}
									});`
	busqueda += `</script>`

	return busqueda
}

//########## GET NAMES ####################################

//GetNameProducto regresa el nombre del Producto con el ID especificado
func GetNameProducto(ID bson.ObjectId) (string, error) {
	var result ProductoMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	err = c.Find(bson.M{"_id": ID}).One(&result)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	return result.Nombre, nil
}

//########################< FUNCIONES GENERALES PSQL >#############################

//######################< FUNCIONES GENERALES ELASTIC >############################

//BuscarEnElastic busca el texto solicitado en los campos solicitados
func BuscarEnElastic(texto string) (*elastic.SearchResult, error) {
	textoTilde, textoQuotes := MoGeneral.ConstruirCadenas(texto)

	queryTilde := elastic.NewQueryStringQuery(textoTilde)
	queryQuotes := elastic.NewQueryStringQuery(textoQuotes)

	queryTilde = queryTilde.Field("Nombre")
	queryQuotes = queryQuotes.Field("Nombre")

	queryTilde = queryTilde.Field("Tipo")
	queryQuotes = queryQuotes.Field("Tipo")

	queryTilde = queryTilde.Field("Codigos.Valores")
	queryQuotes = queryQuotes.Field("Codigos.Valores")

	queryTilde = queryTilde.Field("Etiquetas")
	queryQuotes = queryQuotes.Field("Etiquetas")

	queryTilde = queryTilde.Field("Estatus")
	queryQuotes = queryQuotes.Field("Estatus")

	var docs *elastic.SearchResult
	var err error

	docs, err = MoConexion.ElasticBuscaEspecifica(MoVar.IndexElastic, MoVar.TipoProducto, 0, 1000, queryTilde)
	if err != nil {
		fmt.Println(err)
		fmt.Println("Falló el primer intento con: ", textoTilde)

		docs, err = MoConexion.ElasticBuscaEspecifica(MoVar.IndexElastic, MoVar.TipoProducto, 0, 1000, queryQuotes)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Falló el segundo intento con: ", textoQuotes)
			return nil, err
		}
		if docs.Hits.TotalHits == 0 {
			fmt.Println("No se encontraron resultados en el segundo intento con: ", textoQuotes)
		}
		return docs, nil

	}

	if docs.Hits.TotalHits == 0 {
		fmt.Println("No se encontraron resultados en el primer intento con: ", textoTilde)

		docs, err = MoConexion.ElasticBuscaEspecifica(MoVar.IndexElastic, MoVar.TipoProducto, 0, 1000, queryQuotes)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Falló el segundo intento con: ", textoQuotes)
			return nil, err
		}

		if docs.Hits.TotalHits == 0 {
			fmt.Println("No se encontraron resultados en el segundo intento con: ", textoQuotes)
		}

		return docs, nil
	}

	return docs, nil
}

//BuscarEnElasticProveedor busca el texto solicitado en los campos solicitados
//WebService - Proveedores
func BuscarEnElasticProveedor(texto string) (*elastic.SearchResult, error) {
	textoTilde, textoQuotes := MoGeneral.ConstruirCadenas(texto)

	queryTilde := elastic.NewQueryStringQuery(textoTilde)
	queryQuotes := elastic.NewQueryStringQuery(textoQuotes)

	queryTilde = queryTilde.Field("RFC")
	queryQuotes = queryQuotes.Field("RFC")

	queryTilde = queryTilde.Field("Estatus")
	queryQuotes = queryQuotes.Field("Estatus")

	var docs *elastic.SearchResult
	var err error
	docs, err = MoConexion.ElasticBuscaEspecifica(MoVar.IndexElastic, "Cliente", 0, 1000, queryTilde)

	if err != nil {
		fmt.Println(err)
		fmt.Println("Falló el primer intento con: ", textoTilde)
	} else {
		if docs.Hits.TotalHits == 0 {
			fmt.Println("No se encontraron resultados en el primer intento con: ", textoTilde)
			docs, err = MoConexion.ElasticBuscaEspecifica(MoVar.IndexElastic, "Cliente", 0, 1000, queryQuotes)
			if err != nil {
				fmt.Println(err)
				fmt.Println("Falló el segundo intento con: ", textoQuotes)
			}
		}
	}

	return docs, err
}

//BuscarEnElasticSatDefault busca el texto solicitado en los campos solicitados
func BuscarEnElasticSatDefault(index, tipo, texto, segmento, familia, clase string) (*[]ProductosSat, error) {
	var Productos []ProductosSat
	var Producto ProductosSat

	tilde, quote := MoGeneral.ConstruirCadenas(texto)

	if segmento != "" {
		tilde += " AND " + segmento
		quote += " AND " + segmento
	}

	if familia != "" {
		tilde += " AND " + familia
		quote += " AND " + familia
	}

	if clase != "" {
		tilde += " AND " + clase
		quote += " AND " + clase
	}

	fmt.Println("cadenas: ", tilde, " y ", quote)

	queryTilde := elastic.NewQueryStringQuery(tilde)
	queryTilde = queryTilde.Field("descripcion")
	queryTilde = queryTilde.Field("claveprodserv")

	if segmento != "" {
		queryTilde = queryTilde.Field("codigosegmento")
		//queryTilde = queryTilde.FieldWithBoost("codigosegmento", 2)
	}

	if familia != "" {
		queryTilde = queryTilde.Field("codigofamilia")
		//queryTilde = queryTilde.FieldWithBoost("codigofamilia", 2)
	}

	if clase != "" {
		queryTilde = queryTilde.Field("codigoclase")
		//queryTilde = queryTilde.FieldWithBoost("codigoclase", 2)
	}

	docs, err := MoConexion.ElasticBuscaEspecifica(index, tipo, 0, 10000, queryTilde)
	if err != nil {
		fmt.Println(err)
		return &Productos, err
	}

	if docs.Hits.TotalHits > 0 {
		for _, v := range docs.Hits.Hits {
			Producto = ProductosSat{}
			err := json.Unmarshal(*v.Source, &Producto)
			if err == nil {
				Productos = append(Productos, Producto)
			} else {
				fmt.Println("Unmarchall Error: ", err)
			}
		}
	} else {

		queryQuote := elastic.NewQueryStringQuery(quote)
		queryQuote = queryQuote.Field("descripcion")
		queryQuote = queryQuote.Field("claveprodserv")

		if segmento != "" {
			queryQuote = queryQuote.Field("codigosegmento")
			//queryQuote = queryQuote.FieldWithBoost("codigosegmento", 2)
		}

		if familia != "" {
			queryQuote = queryQuote.Field("codigofamilia")
			//queryQuote = queryQuote.FieldWithBoost("codigofamilia", 2)
		}

		if clase != "" {
			queryQuote = queryQuote.Field("codigoclase")
			//queryQuote = queryQuote.FieldWithBoost("codigoclase", 2)
		}

		docs, err := MoConexion.ElasticBuscaEspecifica(index, tipo, 0, 10000, queryQuote)
		if err != nil {
			fmt.Println(err)
			return &Productos, err
		}

		if docs.Hits.TotalHits > 0 {
			for _, v := range docs.Hits.Hits {
				Producto = ProductosSat{}
				err := json.Unmarshal(*v.Source, &Producto)
				if err == nil {
					Productos = append(Productos, Producto)
				} else {
					fmt.Println("Unmarchall Error: ", err)
				}
			}
		}
	}

	return &Productos, nil
}

//GeneraTemplatesBusquedaSat crea templates de tabla de búsqueda
func GeneraTemplatesBusquedaSat(Productos []ProductosSat) (string, string) {

	cuerpo := ``

	cabecera := `<tr>
				<th>#</th>
				<th><small>Selecciona</small></th>
				<th><small>Código</small></th>
				<th><small>Descripción</small></th>			
				</tr>`

	for k, v := range Productos {
		cuerpo += `<tr>`

		cuerpo += `<td>` + strconv.Itoa(k+1) + `</td>`

		cuerpo += `<td class="text-center"><input class="btn btn-primary ProdAsignar"  type="button" id="` + v.Clave + `" value="Seleccionar" name="mismo" onClick="AsignarClaveSat(this.id)"></td>`
		cuerpo += `<td><small>
							<div class="popover-markup">
								<a class="Impuestolink trigger">` + v.Clave + `</a>
								<div class="head hide"><h4>Informaci&oacute;n de C&oacute;digo</h4></div>
								<div class="content hide">
									<table class="table table-hover table-striped table-condensed table-bordered">
										<thead>
											<tr class="info">
												<th>Tipo</th>
												<th>C&oacute;digo</th>
												<th>Descripci&oacute;n</th>
											</tr>
										</thead>
										<tbody>
											<tr class="">
												<th>Segmento</th>
												<td>` + v.CodigoSegmento + `</td>
												<td><em>` + v.NombreSegmento + `</em></td>
											</tr>
											<tr class="">
												<th>Familia</th>
												<td>` + v.CodigoFamilia + `</td>
												<td><em>` + v.NombreFamilia + `</em></td>
											</tr>
											<tr class="">
												<th>Clase</th>
												<td>` + v.CodigoClase + `</td>
												<td><em>` + v.NombreClase + `</em></td>
											</tr>
										</tbody>
									</table>								
								</div>						
							</div>
							</small>
					   </td>`
		cuerpo += `<td><small>` + strings.ToUpper(v.Descripcion) + `</small></td>`
		cuerpo += `</tr>`

	}

	return cabecera, cuerpo
}

//CargaComboSegmento carga combo de segmentos
func CargaComboSegmento(Index, tipo, valor, campo string) (string, error) {
	html := ``
	html += `<option value="" selected>--SELECCIONE--</option>`
	docs, err := MoConexion.ElasticConsultaIndexSat(Index, tipo, valor, campo)
	if err != nil {
		return html, err
	}

	var SSegmento Segmento

	if docs.Hits.TotalHits > 0 {
		for _, v := range docs.Hits.Hits {
			SSegmento = Segmento{}
			err := json.Unmarshal(*v.Source, &SSegmento)
			if err == nil {
				html += `<option value="` + SSegmento.Clave + `">` + SSegmento.Clave + `:` + SSegmento.Nombre + `</option>`
			} else {
				fmt.Println("Unmarchall Error: ", err)
			}
		}
	}

	return html, nil
}

//CargaComboClase carga combo de segmentos
func CargaComboClase(Index, tipo, valor, campo string) (string, error) {
	html := ``
	html += `<option value="" selected>--SELECCIONE--</option>`
	docs, err := MoConexion.ElasticConsultaIndexSat(Index, tipo, valor, campo)
	if err != nil {
		return html, err
	}

	var SClase Clase

	if docs.Hits.TotalHits > 0 {
		for _, v := range docs.Hits.Hits {
			SClase = Clase{}
			err := json.Unmarshal(*v.Source, &SClase)
			if err == nil {
				html += `<option value="` + SClase.Clave + `">` + SClase.Clave + `:` + SClase.Nombre + `</option>`
			} else {
				fmt.Println("Unmarchall Error: ", err)
			}
		}
	}
	return html, nil
}

//CargaComboFamilia carga combo de segmentos
func CargaComboFamilia(Index, tipo, valor, campo string) (string, error) {
	html := ``
	html += `<option value="" selected>--SELECCIONE--</option>`
	docs, err := MoConexion.ElasticConsultaIndexSat(Index, tipo, valor, campo)
	if err != nil {
		return html, err
	}

	var SFamilia Familia

	if docs.Hits.TotalHits > 0 {
		for _, v := range docs.Hits.Hits {
			SFamilia = Familia{}
			err := json.Unmarshal(*v.Source, &SFamilia)
			if err == nil {
				html += `<option value="` + SFamilia.Clave + `" >` + SFamilia.Clave + `:` + SFamilia.Nombre + `</option>`
			} else {
				fmt.Println("Unmarchall Error: ", err)
			}
		}
	}

	return html, nil
}

//################################################<<METODOS DE GESTION >>################################################################

//##################################<< INSERTAR >>###################################

//InsertaMgo es un método que crea un registro en Mongo
func (p ProductoMgo) InsertaMgo() error {
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	err = c.Insert(p)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

//InsertaElastic es un método que crea un registro en Mongo
func (p ProductoMgo) InsertaElastic() error {
	var ProductoE ProductoElastic

	ProductoE.Nombre = p.Nombre
	ProductoE.Codigos.Claves = p.Codigos.Claves
	ProductoE.Codigos.Valores = p.Codigos.Valores
	ProductoE.Tipo = p.Tipo.Hex()
	//ProductoE.CodigoSat = p.CodigoSat
	ProductoE.Etiquetas = p.Etiquetas
	ProductoE.Estatus = p.Estatus.Hex()

	err := MoConexion.ElasticInserta(MoVar.IndexElastic, MoVar.TipoProducto, p.ID.Hex(), ProductoE)
	if err != nil {
		return err
	}
	return nil
}

//##########################<< UPDATE >>############################################

//ActualizaMgo es un método que encuentra y Actualiza un registro en Mongo
//IMPORTANTE --> Debe coincidir el número y orden de campos con el de valores
func (p ProductoMgo) ActualizaMgo(campos []string, valores []interface{}) error {

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	var Abson bson.M
	Abson = make(map[string]interface{})
	for k, v := range campos {
		Abson[v] = valores[k]
	}
	change := bson.M{"$set": Abson}
	err = c.Update(bson.M{"_id": p.ID}, change)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

//ActualizaElastic es un método que encuentra y Actualiza un registro en Mongo
func (p ProductoMgo) ActualizaElastic() error {
	err := MoConexion.ElasticActualiza(MoVar.IndexElastic, MoVar.TipoProducto, p.ID.Hex(), p)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

//##########################<< REEMPLAZA >>############################################

//ReemplazaMgo es un método que encuentra y Actualiza un registro en Mongo
func (p ProductoMgo) ReemplazaMgo() error {

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	err = c.Update(bson.M{"_id": p.ID}, p)
	if err != nil {
		return err
	}

	return nil
}
