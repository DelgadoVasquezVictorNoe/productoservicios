package ProductoModel

import (
	"html/template"
	"time"

	"gopkg.in/mgo.v2/bson"
)

//#########################< ESTRUCTURAS >##############################

//ProductoMgo estructura de Productos mongo
type ProductoMgo struct {
	ID                  bson.ObjectId   `bson:"_id,omitempty"`
	Nombre              string          `bson:"Nombre"`
	Codigos             Codigos         `bson:"Codigos"`
	Tipo                bson.ObjectId   `bson:"Tipo,omitempty"`
	Kit                 []DataKitMgo    `bson:"Kit"`
	Imagenes            []bson.ObjectId `bson:"Imagenes,omitempty"`
	Unidad              UnidadMgo       `bson:"Unidad"`
	CodigoSat           CodigoSatMgo    `bson:"CodigoSat"`
	Proveedor           ProveedorMgo    `bson:"Proveedor"`
	Impuestos           []Impuestos     `bson:"Impuestos"`
	VentaFraccionCompra bool            `bson:"VentaFraccionCompra"`
	VentaFraccionVenta  bool            `bson:"VentaFraccionVenta"`
	Etiquetas           []string        `bson:"Etiquetas"`
	Estatus             bson.ObjectId   `bson:"Estatus,omitempty"`
	FechaHora           time.Time       `bson:"FechaHora"`
}

//ProductoElastic estructura de Productos para insertar en Elastic
type ProductoElastic struct {
	ID        string           `bson:"_id,omitempty"`
	Nombre    string           `json:"Nombre_Producto"`
	Codigos   CodigosElastic   `json:"Codigos_Producto"`
	Tipo      string           `json:"Tipo_Producto,omitempty"`
	CodigoSat CodigoSatMgo     `json:"CodigoSat_Producto"`
	Kit       []DataKitElastic `json:"Kit_Producto"`
	Unidad    UnidadElastic    `json:"Unidades_Producto"`
	Etiquetas []string         `json:"Etiquetas_Producto"`
	Estatus   string           `json:"Estatus_Producto"`
}

//Impuestos subestructura de Producto
type Impuestos struct {
	Clave       string `bson:"Clave"`
	Descripcion string `bson:"Descripcion"`
}

//DataKitMgo subestructura de Producto
type DataKitMgo struct {
	Producto bson.ObjectId `bson:"Producto,omitempty"`
	Cantidad float64       `bson:"Cantidad"`
}

//DataKitElastic subestructura de Producto
type DataKitElastic struct {
	Producto string `json:"Producto"`
	Cantidad string `json:"Cantidad"`
}

//CodigosElastic subestructura de Producto
type CodigosElastic struct {
	Claves  []string `json:"Claves_Producto"`
	Valores []string `json:"Valores_Producto"`
}

//Codigos subestructura de ProductoElastic
type Codigos struct {
	Claves  []string `bson:"Claves"`
	Valores []string `bson:"Valores"`
}

//UnidadElastic subestructura de Producto
type UnidadElastic struct {
	Default string   `json:"Default_Producto"`
	Valores []string `json:"Valores_Producto"`
}

//UnidadMgo subestructura de Producto
type UnidadMgo struct {
	Default bson.ObjectId      `bson:"Default,omitempty"`
	Valores []UnidadValoresMgo `bson:"Valores"`
}

//UnidadValoresMgo subestructura de Producto
type UnidadValoresMgo struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	Factor     float64       `bson:"Factor"`
	Porcentaje float64       `bson:"Porcentaje"`
	TipoFactor string        `bson:"TipoFactor"`
	Regresable bool          `bson:"Regresable"`
	Compra     bool          `bson:"Compra"`
	Venta      bool          `bson:"Venta"`
}

//CodigoSatMgo subestructura de Producto
type CodigoSatMgo struct {
	Default string   `json:"Default"`
	Valores []string `json:"Valores"`
}

//ProveedorMgo subestructura de Producto
type ProveedorMgo struct {
	Default bson.ObjectId   `json:"Default,omitempty"`
	Valores []bson.ObjectId `json:"Valores,omitempty"`
}

//ENombreProducto Estructura de campo de Producto
type ENombreProducto struct {
	Nombre   string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//ECodigosProducto Estructura de campo de Producto
type ECodigosProducto struct {
	Codigos  Codigo
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//ETipoProducto Estructura de campo de Producto
type ETipoProducto struct {
	Tipo     bson.ObjectId
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EKitProducto Estructura de campo de Producto
type EKitProducto struct {
	Kit      string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EImagenesProducto Estructura de campo de Producto
type EImagenesProducto struct {
	Imagenes []bson.ObjectId
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EUnidadProducto Estructura de campo de Producto
type EUnidadProducto struct {
	Unidad   Unidad
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EUnidadVentaProducto Estructura de campo de Producto
/*type EUnidadVentaProducto struct {
	UnidadVenta Unidad
	IEstatus    bool
	IMsj        string
	Ihtml       template.HTML
}*/

//EUnidadCompraProducto Estructura de campo de Producto
/*type EUnidadCompraProducto struct {
	UnidadCompra Unidad
	IEstatus     bool
	IMsj         string
	Ihtml        template.HTML
}*/

//ECodigoSatProducto Estructura de campo de Producto
type ECodigoSatProducto struct {
	CodigoSat string
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//EProveedorProducto Estructura de campo de Producto
type EProveedorProducto struct {
	Proveedor Proveedor
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//EImpuestosProducto Estructura de campo de Producto
type EImpuestosProducto struct {
	Impuestos string
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//ECodigosVistaProducto Estructura de campo de Producto
type ECodigosVistaProducto struct {
	Codigos  string
	Segmento template.HTML
	Familia  template.HTML
	Grupo    template.HTML
	Clase    template.HTML
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EVentaFraccionCompraProducto Estructura de campo de Producto
type EVentaFraccionCompraProducto struct {
	VentaFraccionCompra bool
	IEstatus            bool
	IMsj                string
	Ihtml               template.HTML
}

//EVentaFraccionVentaProducto Estructura de campo de Producto
type EVentaFraccionVentaProducto struct {
	VentaFraccionVenta bool
	IEstatus           bool
	IMsj               string
	Ihtml              template.HTML
}

//EEtiquetasProducto Estructura de campo de Producto
type EEtiquetasProducto struct {
	Etiquetas []string
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//EEstatusProducto Estructura de campo de Producto
type EEstatusProducto struct {
	Estatus  bson.ObjectId
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EFechaHoraProducto Estructura de campo de Producto
type EFechaHoraProducto struct {
	FechaHora time.Time
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//Producto estructura de Productos mongo
type Producto struct {
	ID bson.ObjectId
	ENombreProducto
	ECodigosProducto
	ETipoProducto
	EKitProducto
	EImagenesProducto
	//EUnidadVentaProducto
	//EUnidadCompraProducto
	EUnidadProducto
	ECodigoSatProducto
	EProveedorProducto
	EImpuestosProducto
	ECodigosVistaProducto
	EVentaFraccionCompraProducto
	EVentaFraccionVentaProducto
	EEtiquetasProducto
	EEstatusProducto
	EFechaHoraProducto
}

//SSesion estructura de variables de sesion de Usuarios del sistema
type SSesion struct {
	Name          string
	MenuPrincipal template.HTML
	MenuUsr       template.HTML
}

//SIndex estructura de variables de index
type SIndex struct {
	SResultados        bool
	SRMsj              string
	STituloTabla       string
	SUrlDeDatos        string
	SNombresDeColumnas []string
	SModeloDeColumnas  []map[string]interface{}
	SRenglones         map[string]interface{}
}

//SProducto estructura de Producto para la vista
type SProducto struct {
	ID        string
	SEstado   bool
	SMsj      string
	SIhtml    template.HTML
	SElastic  bool
	SBusqueda template.HTML
	Producto
	SIndex
	SSesion
}

//SProveedor estructura de Producto para la vista
type SProveedor struct {
	ID        string
	SEstado   bool
	SMsj      string
	SIhtml    template.HTML
	SElastic  bool
	SBusqueda template.HTML
	SIndex
	SSesion
}

//EClavesCodigos Estructura de campo de Codigos
type EClavesCodigos struct {
	Claves   []string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EValoresCodigos Estructura de campo de Codigos
type EValoresCodigos struct {
	Valores  []string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//Codigo subestructura de Producto
type Codigo struct {
	EClavesCodigos
	EValoresCodigos
}

//EDefaultUnidadVenta Estructura de campo de UnidadVenta
/*type EDefaultUnidadVenta struct {
	Default  string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}*/

//EDefaultUnidad Estructura de campo de UnidadVenta
type EDefaultUnidad struct {
	Default  string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EDefaultProveedor Estructura de campo de Proveedor
type EDefaultProveedor struct {
	Default  string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EValoresUnidadVenta Estructura de campo de UnidadVenta
/*type EValoresUnidadVenta struct {
	Valores  []bson.ObjectId
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}*/

//EValoresUnidad Estructura de campo de UnidadVenta
type EValoresUnidad struct {
	Valores  []bson.ObjectId
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EValoresProveedor Estructura de campo de Proveedor
type EValoresProveedor struct {
	Valores  []bson.ObjectId
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//Unidad subestructura de Producto
/*type Unidad struct {
	EDefaultUnidadVenta
	EValoresUnidadVenta
}*/

//Unidad subestructura de Producto
type Unidad struct {
	EDefaultUnidad
	EValoresUnidad
}

//Proveedor subestructura de Producto
type Proveedor struct {
	EDefaultProveedor
	EValoresProveedor
}

//SDataProducto estructura de Productos para la vista
type SDataProducto struct {
	ID        string
	SEstado   bool
	SMsj      string
	SIhtml    template.HTML
	SElastic  bool
	SBusqueda template.HTML
}

//ListaVista estructura Auxiliar para enviar listas a la vista
type ListaVista struct {
	ID          string
	SEstado     bool
	SMsj        string
	SIhtml      template.HTML
	SCabecera   template.HTML
	SBody       template.HTML
	SPaginacion template.HTML
	SGrupo      template.HTML
	SSegmento   template.HTML
	SFamilia    template.HTML
	SClase      template.HTML
}

//ProductosSat objeto que se encarga de tratar los datos obtenidos de elasticsearch
type ProductosSat struct {
	Clave          string `json:"claveprodserv"`
	Descripcion    string `json:"descripcion"`
	CodigoClase    string `json:"codigoclase"`
	NombreClase    string `json:"nombreclase"`
	CodigoFamilia  string `json:"codigofamilia"`
	NombreFamilia  string `json:"nombrefamilia"`
	CodigoSegmento string `json:"codigosegmento"`
	NombreSegmento string `json:"nombresegmento"`
}

//Segmento objeto segmento de elastic
type Segmento struct {
	Nombre string `json:"nombresegmento"`
	Clave  string `json:"codigosegmento"`
}

//Familia objeto Familia de elastic
type Familia struct {
	Nombre string `json:"nombrefamilia"`
	Clave  string `json:"codigofamilia"`
}

//Clase objeto clase de elastic
type Clase struct {
	Nombre string `json:"nombreclase"`
	Clave  string `json:"codigoclase"`
}
