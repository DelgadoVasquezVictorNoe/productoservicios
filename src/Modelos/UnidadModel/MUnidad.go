package UnidadModel

import (
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"../../Modelos/CatalogoModel"

	"../../Modulos/Conexiones"
	"../../Modulos/General"
	"../../Modulos/Variables"

	"gopkg.in/mgo.v2/bson"
	"gopkg.in/olivere/elastic.v5"
)

//#########################< FUNCIONES GENERALES MGO >###############################

//GetAll Regresa todos los documentos existentes de Mongo (Por Coleccion)
func GetAll() (*[]UnidadMgo, error) {
	var result []UnidadMgo

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	err = c.Find(nil).All(&result)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return &result, nil
}

//CountAll Regresa todos los documentos existentes de Mongo (Por Coleccion)
func CountAll() (int, error) {
	var result int
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return 0, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	result, err = c.Find(nil).Count()
	if err != nil {
		fmt.Println(err)
		return 0, err
	}

	return result, nil
}

//GetOne Regresa un documento específico de Mongo (Por Coleccion)
func GetOne(ID bson.ObjectId) (UnidadMgo, error) {
	var result UnidadMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	err = c.Find(bson.M{"_id": ID}).One(&result)
	if err != nil {
		fmt.Println(err)
		return result, err
	}

	return result, nil
}

//GetEspecifics rsegresa un conjunto de documentos específicos de Mongo (Por Coleccion)
func GetEspecifics(Ides []bson.ObjectId) ([]UnidadMgo, error) {
	var result []UnidadMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	In := bson.M{"$in": Ides}
	err = c.Find(bson.M{"_id": In}).All(&result)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return result, nil
}

//GetEspecificByFields regresa un documento de Mongo especificando un campo y un determinado valor
func GetEspecificByFields(field string, valor interface{}) (UnidadMgo, error) {
	var result UnidadMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return result, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	err = c.Find(bson.M{field: valor}).One(&result)
	if err != nil {
		fmt.Println(err)
		return result, err
	}

	return result, nil
}

//GetIDByField regresa un documento específico de Mongo (Por Coleccion)
func GetIDByField(field string, valor interface{}) (bson.ObjectId, error) {
	var result UnidadMgo
	var ID bson.ObjectId

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return ID, err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionProducto)

	err = c.Find(bson.M{field: valor}).One(&result)
	if err != nil {
		fmt.Println(err)
		return ID, err
	}

	return result.ID, nil
}

//MapaModelo regresa el mapa del modelo para jqgrid
func MapaModelo() map[string]interface{} {
	var ModelOptions map[string]interface{}
	ModelOptions = make(map[string]interface{})
	ModelOptions["name"] = []string{"id", "Nombre", "Abreviatura", "CodigoSat", "Estatus", "FechaHora"}
	ModelOptions["index"] = []string{"id", "Nombre", "Abreviatura", "CodigoSat", "Estatus", "FechaHora"}
	ModelOptions["align"] = []string{"left", "left", "left", "left", "left", "left"}
	ModelOptions["editable"] = []bool{false, false, false, false, false, false}
	ModelOptions["hidden"] = []bool{true, false, false, false, false, false}
	ModelOptions["search"] = []bool{true, true, true, true, true, true}
	ModelOptions["stype"] = []string{"text", "text", "text", "text", "select", "text"}
	ModelOptions["sorteable"] = []bool{true, true, true, true, true, true}
	ModelOptions["sorttype"] = []string{"text", "text", "text", "text", "text", "date"}

	var Textos map[string]interface{}
	Textos = make(map[string]interface{})
	Textos["sopt"] = []string{"eq", "ne", "cn"}

	var Numeros map[string]interface{}
	Numeros = make(map[string]interface{})
	Numeros["sopt"] = []string{"eq", "ne", "lt", "le", "gt", "ge"}

	var Fechas map[string]interface{}
	Fechas = make(map[string]interface{})
	Fechas["sopt"] = []string{"eq", "ne", "lt", "le", "gt", "ge"}
	Fechas["dataInit"] = `datePick`

	var Formatos map[string]interface{}
	Formatos = make(map[string]interface{})
	Formatos["srcformat"] = "d m y"
	Formatos["newformat"] = "d m y"

	Fechas["formatoptions"] = Formatos

	var Atributos map[string]interface{}
	Atributos = make(map[string]interface{})
	Atributos["title"] = "Seleccionar Fecha"

	Fechas["attr"] = Atributos

	var ComboEstatus map[string]interface{}
	ComboEstatus = make(map[string]interface{})
	ComboEstatus["sopt"] = []string{"eq", "ne", "cn"}
	ComboEstatus["value"] = CargaComboCatalogoGrid(165)

	ModelOptions["searchoptions"] = []interface{}{Textos, Textos, Textos, Textos, ComboEstatus, Fechas}

	return ModelOptions
}

//GeneraRenglonesIndex crea un mapa para crear el json de renglones del index
func GeneraRenglonesIndex(Unidades []UnidadMgo) map[string]interface{} {
	var Mapas []map[string]interface{}
	var DataM = MoVar.CargaSeccionCFG(MoVar.SecMongo)

	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer conn.Close()

	for _, v := range Unidades {

		var Mapa map[string]interface{}
		Mapa = make(map[string]interface{})
		Mapa["id"] = v.ID.Hex()
		Mapa["Nombre"] = v.Nombre
		Mapa["Abreviatura"] = v.Abreviatura
		Mapa["CodigoSat"] = v.CodigoSat

		var Estatus string
		var Catalogo CatalogoModel.CatalogoMgo
		err = conn.Session.DB(DataM.NombreBase).C("Catalogo").Find(bson.M{"Valores._id": v.Estatus}).One(&Catalogo)
		if err != nil {
			fmt.Println(err)
		}

		valores := Catalogo.Valores

		for _, w := range valores {
			if v.Estatus.Hex() == w.ID.Hex() {
				Estatus = w.Valor
			}
		}

		Mapa["Estatus"] = Estatus
		Mapa["FechaHora"] = v.FechaHora.Format("2006/1/2")
		Mapas = append(Mapas, Mapa)

	}

	var Mapita map[string]interface{}
	Mapita = make(map[string]interface{})
	Mapita["rows"] = Mapas

	return Mapita

}

//########## GET NAMES ####################################

//GetNameUnidad regresa el nombre del Unidad con el ID especificado
func GetNameUnidad(ID bson.ObjectId) (string, error) {
	var result UnidadMgo
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	err = c.Find(bson.M{"_id": ID}).One(&result)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	return result.Nombre, nil
}

//########################< FUNCIONES GENERALES PSQL >#############################

//######################< FUNCIONES GENERALES ELASTIC >############################

//BuscarEnElastic busca el texto solicitado en los campos solicitados
func BuscarEnElastic(texto string) (*elastic.SearchResult, error) {
	textoTilde, textoQuotes := MoGeneral.ConstruirCadenas(texto)

	queryTilde := elastic.NewQueryStringQuery(textoTilde)
	queryQuotes := elastic.NewQueryStringQuery(textoQuotes)

	queryTilde = queryTilde.Field("Nombre")
	queryQuotes = queryQuotes.Field("Nombre")

	queryTilde = queryTilde.Field("Abreviatura")
	queryQuotes = queryQuotes.Field("Abreviatura")

	queryTilde = queryTilde.Field("CodigoSat")
	queryQuotes = queryQuotes.Field("CodigoSat")

	queryTilde = queryTilde.Field("Estatus")
	queryQuotes = queryQuotes.Field("Estatus")

	var docs *elastic.SearchResult
	var err error

	docs, err = MoConexion.ElasticBuscaEspecifica(MoVar.IndexElastic, MoVar.TipoUnidad, 0, 1000, queryTilde)
	if err != nil {
		fmt.Println(err)
		fmt.Println("Falló el primer intento con: ", textoTilde)

		docs, err = MoConexion.ElasticBuscaEspecifica(MoVar.IndexElastic, MoVar.TipoUnidad, 0, 1000, queryTilde)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Falló el segundo intento con: ", textoQuotes)
			return nil, err
		}
		if docs.Hits.TotalHits == 0 {
			fmt.Println("No se encontraron resultados en el segundo intento con: ", textoQuotes)
		}
		return docs, nil
	}

	if docs.Hits.TotalHits == 0 {
		fmt.Println("No se encontraron resultados en el primer intento con: ", textoTilde)
		docs, err = MoConexion.ElasticBuscaEspecifica(MoVar.IndexElastic, MoVar.TipoUnidad, 0, 1000, queryTilde)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Falló el segundo intento con: ", textoQuotes)
			return nil, err
		}

		if docs.Hits.TotalHits == 0 {
			fmt.Println("No se encontraron resultados en el segundo intento con: ", textoQuotes)
		}
		return docs, nil
	}
	return docs, nil
}

//BuscarEnElasticSatDefault busca el texto solicitado en los campos solicitados
func BuscarEnElasticSatDefault(index, tipo, texto string) (*[]UnidadesSat, error) {
	var Unidades []UnidadesSat
	var Unidad UnidadesSat

	tilde, quote := MoGeneral.ConstruirCadenas(texto)

	queryTilde := elastic.NewQueryStringQuery(tilde)

	queryTilde = queryTilde.Field("claveunidad")
	//queryTilde = queryTilde.Field("descripcion")
	queryTilde = queryTilde.Field("nombre")

	docs, err := MoConexion.ElasticBuscaEspecifica(index, tipo, 0, 10000, queryTilde)
	if err != nil {
		fmt.Println(err)
		return &Unidades, err
	}

	if docs.Hits.TotalHits > 0 {
		for _, v := range docs.Hits.Hits {
			Unidad = UnidadesSat{}
			err := json.Unmarshal(*v.Source, &Unidad)
			if err == nil {
				Unidades = append(Unidades, Unidad)
			} else {
				fmt.Println("Unmarchall Error: ", err)
			}
		}
	} else {

		queryQuote := elastic.NewQueryStringQuery(quote)
		queryQuote = queryQuote.Field("nombre")
		queryQuote = queryQuote.Field("descripcion")
		queryQuote = queryQuote.Field("claveunidad")

		docs, err := MoConexion.ElasticBuscaEspecifica(index, tipo, 0, 10000, queryQuote)
		if err != nil {
			fmt.Println(err)
			return &Unidades, err
		}

		if docs.Hits.TotalHits > 0 {
			for _, v := range docs.Hits.Hits {
				Unidad = UnidadesSat{}
				err := json.Unmarshal(*v.Source, &Unidad)
				if err == nil {
					Unidades = append(Unidades, Unidad)
				} else {
					fmt.Println("Unmarchall Error: ", err)
				}
			}
		}
	}

	return &Unidades, nil
}

//GeneraTemplatesBusquedaSat crea templates de tabla de búsqueda
func GeneraTemplatesBusquedaSat(Unidades []UnidadesSat) (string, string) {

	cuerpo := ``

	cabecera := `<tr>
				<th>#</th>
				<th><small>Selecciona</small></th>
				<th><small>Clave</small></th>
				<th><small>Nombre</small></th>			
				<th><small>Símbolo</small></th>			
				</tr>`

	for k, v := range Unidades {
		cuerpo += `<tr>`

		cuerpo += `<td>` + strconv.Itoa(k+1) + `</td>`

		cuerpo += `<td class="text-center"><input class="btn btn-primary btn-sm ProdAsignar"  type="button" id="` + v.Clave + `" value="Seleccionar" descripcion="` + v.Descripcion + `" nombre="` + v.Nombre + `" simbolo="` + v.Simbolo + `" name="mismo" onClick="AsignarClaveSat(this)"></td>`
		cuerpo += `<td><small>
							<div class="popover-markup">
								<a class="Impuestolink trigger">` + v.Clave + `</a>
								<div class="head hide"><h4>Informaci&oacute;n de C&oacute;digo</h4></div>
								<div class="content hide">
									<div><span>` + v.Descripcion + `</span></div>
								</div>						
							</div>
							</small>
					   </td>`
		cuerpo += `<td><small>` + strings.ToUpper(v.Nombre) + `</small></td>`
		cuerpo += `<td><small>` + strings.ToUpper(v.Simbolo) + `</small></td>`
		cuerpo += `</tr>`

	}

	return cabecera, cuerpo
}

//################################################<<METODOS DE GESTION >>################################################################

//##################################<< INSERTAR >>###################################

//InsertaMgo es un método que crea un registro en Mongo
func (p UnidadMgo) InsertaMgo() error {
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	err = c.Insert(p)
	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}

//InsertaElastic es un método que crea un registro en Mongo
func (p UnidadMgo) InsertaElastic() error {
	var UnidadE UnidadElastic

	UnidadE.Nombre = p.Nombre
	UnidadE.Abreviatura = p.Abreviatura
	UnidadE.CodigoSat = p.CodigoSat
	UnidadE.Estatus = p.Estatus
	UnidadE.FechaHora = p.FechaHora
	err := MoConexion.ElasticInserta(MoVar.IndexElastic, MoVar.TipoUnidad, p.ID.Hex(), UnidadE)
	if err != nil {
		return err
	}
	return nil
}

//##########################<< UPDATE >>############################################

//ActualizaMgo es un método que encuentra y Actualiza un registro en Mongo
//IMPORTANTE --> Debe coincidir el número y orden de campos con el de valores
func (p UnidadMgo) ActualizaMgo(campos []string, valores []interface{}) error {
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	var Abson bson.M
	Abson = make(map[string]interface{})
	for k, v := range campos {
		Abson[v] = valores[k]
	}
	change := bson.M{"$set": Abson}
	err = c.Update(bson.M{"_id": p.ID}, change)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

//ActualizaElastic es un método que encuentra y Actualiza un registro en Mongo
func (p UnidadMgo) ActualizaElastic() error {
	err := MoConexion.ElasticActualiza(MoVar.IndexElastic, MoVar.TipoUnidad, p.ID.Hex(), p)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

//##########################<< REEMPLAZA >>############################################

//ReemplazaMgo es un método que encuentra y Actualiza un registro en Mongo
func (p UnidadMgo) ReemplazaMgo() error {
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	err = c.Update(bson.M{"_id": p.ID}, p)
	if err != nil {
		return err
	}

	return nil
}

//###########################<< CONSULTA EXISTENCIAS >>###################################

//ConsultaExistenciaByFieldMgo es un método que verifica si un registro existe en Mongo indicando un campo y un valor string
func (p UnidadMgo) ConsultaExistenciaByFieldMgo(field string, valor string) error {
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	n, e := c.Find(bson.M{field: valor}).Count()
	if e != nil {
		fmt.Println(e)
		return e
	}

	if n == 0 {
		return nil
	}

	return errors.New("Ya existe.")
}

//ConsultaExistenciaByIDMgo es un método que encuentra un registro en Mongo buscándolo por ID
func (p UnidadMgo) ConsultaExistenciaByIDMgo() error {
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	n, e := c.Find(bson.M{"_id": p.ID}).Count()
	if e != nil {
		fmt.Println(e)
		return e
	}

	if n == 0 {
		return nil
	}

	return errors.New("Ya existe.")
}

//ConsultaExistenciaByIDElastic es un método que encuentra un registro en Mongo buscándolo por ID
func (p UnidadMgo) ConsultaExistenciaByIDElastic() error {
	result, err := MoConexion.ElasticConsultaExistencia(MoVar.IndexElastic, MoVar.TipoUnidad, p.ID.Hex())
	if err != nil {
		fmt.Println(err)
		return err
	}
	if result {
		return nil
	}
	return errors.New("No se encontró el Registro")
}

//##################################<< ELIMINACIONES >>#################################################

//EliminaByIDMgo es un método que elimina un registro en Mongo
func (p UnidadMgo) EliminaByIDMgo() error {
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer conn.Close()

	c := conn.GetCollection(MoVar.ColeccionUnidad)

	e := c.RemoveId(bson.M{"_id": p.ID})
	if e != nil {
		fmt.Println(e)
		return e
	}
	return nil
}

//EliminaByIDElastic es un método que elimina un registro en Mongo
func (p UnidadMgo) EliminaByIDElastic() error {
	err := MoConexion.ElasticDeleteID(MoVar.IndexElastic, MoVar.TipoUnidad, p.ID.Hex())
	if err != nil {
		return err
	}
	return nil
}

//CargaComboCatalogoGrid recibe la clave del catálogo, el identificador opcional
//y regresa el template del combo del catálogo con el identificador seleccionado si así se desea
func CargaComboCatalogoGrid(Clave int) string {
	templ := ``
	templ = `:TODOS`
	templ += `;`

	Catalogo, _ := CatalogoModel.GetEspecificByFields("Clave", int64(Clave))
	for _, v := range Catalogo.Valores {
		templ += v.Valor + `:` + v.Valor
		templ += `;`
	}

	templ = templ[0 : len(templ)-1]
	return templ
}
