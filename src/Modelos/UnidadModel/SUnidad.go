package UnidadModel

import (
	"html/template"
	"time"

	"gopkg.in/mgo.v2/bson"
)

//#########################< ESTRUCTURAS MGO >##############################

//UnidadMgo estructura de Unidads mongo
type UnidadMgo struct {
	ID          bson.ObjectId `bson:"_id,omitempty"`
	Nombre      string        `bson:"Nombre"`
	Abreviatura string        `bson:"Abreviatura"`
	CodigoSat   string        `bson:"CodigoSat"`
	Estatus     bson.ObjectId `bson:"Estatus,omitempty"`
	FechaHora   time.Time     `bson:"FechaHora"`
}

//UnidadElastic estructura de Unidads para insertar en Elastic
type UnidadElastic struct {
	Nombre      string        `json:"Nombre"`
	Abreviatura string        `json:"Abreviatura"`
	CodigoSat   string        `json:"CodigoSat"`
	Estatus     bson.ObjectId `json:"Estatus,omitempty"`
	FechaHora   time.Time     `json:"FechaHora"`
}

//#########################< ESTRUCTURAS VISTA>##############################

//ENombreUnidad Estructura de campo de Unidad
type ENombreUnidad struct {
	Nombre   string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EAbreviaturaUnidad Estructura de campo de Unidad
type EAbreviaturaUnidad struct {
	Abreviatura string
	IEstatus    bool
	IMsj        string
	Ihtml       template.HTML
}

//ECodigoSatUnidad Estructura de campo de Unidad
type ECodigoSatUnidad struct {
	CodigoSat string
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//ECodigoSatUnidad2 Estructura de campo de Unidad
type ECodigoSatUnidad2 struct {
	CodigoSat string
	Grupo     template.HTML
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//EEstatusUnidad Estructura de campo de Unidad
type EEstatusUnidad struct {
	Estatus  bson.ObjectId
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EFechaHoraUnidad Estructura de campo de Unidad
type EFechaHoraUnidad struct {
	FechaHora time.Time
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//Unidad estructura de Unidads mongo
type Unidad struct {
	ID bson.ObjectId
	ENombreUnidad
	EAbreviaturaUnidad
	ECodigoSatUnidad
	ECodigoSatUnidad2
	EEstatusUnidad
	EFechaHoraUnidad
}

//SSesion estructura de variables de sesion de Usuarios del sistema
type SSesion struct {
	Name          string
	MenuPrincipal template.HTML
	MenuUsr       template.HTML
}

//SIndex estructura de variables de index
type SIndex struct {
	SResultados        bool
	SRMsj              string
	STituloTabla       string
	SUrlDeDatos        string
	SNombresDeColumnas []string
	SModeloDeColumnas  []map[string]interface{}
	SRenglones         map[string]interface{}
}

//SUnidad estructura de Unidad para la vista
type SUnidad struct {
	SEstado bool
	SMsj    string
	Unidad
	SIndex
	SSesion
	SGrupo template.HTML
}

//ListaVista estructura Auxiliar para enviar listas a la vista
type ListaVista struct {
	ID          string
	SEstado     bool
	SMsj        string
	SIhtml      template.HTML
	SCabecera   template.HTML
	SBody       template.HTML
	SPaginacion template.HTML
	SGrupo      template.HTML
}

//UnidadesSat objeto que se encarga de tratar los datos obtenidos de elasticsearch
type UnidadesSat struct {
	Clave       string `json:"claveunidad"`
	Nombre      string `json:"nombre"`
	Descripcion string `json:"descripcion"`
	Simbolo     string `json:"simbolo"`
}
