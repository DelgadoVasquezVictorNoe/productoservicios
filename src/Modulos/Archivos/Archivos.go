package Archivos

import (
	"bytes"
	"encoding/base64"
	"html/template"
	"image/jpeg"
	"image/png"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"

	"../../Modulos/Conexiones"
	"../../Modulos/Variables"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//GetTemplateImg Regresa template de imagenes que hacen referencia al arreglo de ObjectIds
func GetTemplateImg(IDImg []bson.ObjectId) (template.HTML, error) {
	htmlImagenes := ``
	for _, id := range IDImg {
		gridfile, err := MoConexion.MongoGetFile(id)
		if err != nil {
			return template.HTML(""), err
		}

		ruta, err := GetSrcImagen(gridfile)
		if err != nil {
			return template.HTML(""), err
		}

		htmlImagenes += `<img id="Imagen" alt="Responsive image" name="ImagenProducto" width="200px" height="200px";" src="` + ruta + `">`
	}
	return template.HTML(htmlImagenes), nil
}

//GetStrImg Regresa template de imagenes que hacen referencia al arreglo de ObjectIds
func GetStrImg(IDImg []bson.ObjectId) (string, string, error) {
	htmlImagenes := ``
	ruta := ``

	for _, id := range IDImg {
		gridfile, err := MoConexion.MongoGetFile(id)
		if err != nil {
			return "", "", err
		}

		rutax, err := GetSrcImagen(gridfile)
		if err != nil {
			return "", "", err
		}
		ruta = rutax
		htmlImagenes += `<img id="Imagen" alt="Responsive image" name="ImagenProducto" width="200px" height="200px";" src="` + ruta + `">`
	}

	return htmlImagenes, ruta, nil
}

//GetSrcImagen obtiene src de una imagen GridFile
func GetSrcImagen(ImgFile *mgo.GridFile) (string, error) {
	var tmp string

	b := make([]byte, ImgFile.Size())
	_, err := ImgFile.Read(b)
	if err != nil {
		return "", err
	}

	switch extension := filepath.Ext(ImgFile.Name()); extension {

	case ".jpg", ".jpeg":

		imagen, _ := jpeg.Decode(bytes.NewReader(b))
		buffer := new(bytes.Buffer)
		if err := jpeg.Encode(buffer, imagen, nil); err != nil {
			return "", err
		}
		str := base64.StdEncoding.EncodeToString(buffer.Bytes())
		tmp = `data:image/jpg;base64,` + str

	case ".png":

		imagen, _ := png.Decode(bytes.NewReader(b))
		buffer := new(bytes.Buffer)
		if err := png.Encode(buffer, imagen); err != nil {
			return "", err
		}
		str := base64.StdEncoding.EncodeToString(buffer.Bytes())
		tmp = `data:image/png;base64,` + str
	}

	return tmp, nil
}

//ActualizaArchivo actualiza archivo en el servidor y regresa el path de este.
func ActualizaArchivo(file multipart.File, header *multipart.FileHeader) (string, error) {
	defer file.Close()

	filename := header.Filename
	dirpath := MoVar.CarpetaDeImagenes

	if _, err := os.Stat(dirpath); os.IsNotExist(err) {
		os.MkdirAll(dirpath, 0777)
	}

	out, err := os.Create(dirpath + "/" + filename)
	if err != nil {
		return "", err
	}
	defer out.Close()

	_, err = io.Copy(out, file)
	if err != nil {
		return "", err
	}

	return dirpath, nil
}
