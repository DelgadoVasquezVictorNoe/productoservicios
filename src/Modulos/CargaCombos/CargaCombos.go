package CargaCombos

import (
	"strconv"

	"../../Modelos/CatalogoModel"

	"../../Modelos/UnidadModel"
	"gopkg.in/mgo.v2/bson"
)

//CargaComboSexo carga el combo de sexos
func CargaComboSexo(SexoSelect string) string {
	var Sexos = []string{"Masculino", "Femenino"}
	templ := ``
	for _, Sexo := range Sexos {
		if Sexo == SexoSelect {
			templ += `<option value="` + Sexo + `" selected>` + Sexo + `</option>`
		} else {
			templ += `<option value="` + Sexo + `">` + Sexo + `</option>`
		}
	}
	return templ
}

//CargaComboMostrarEnIndex carga las opciones de mostrar en el index
func CargaComboMostrarEnIndex(Muestra int) string {
	var Cantidades = []int{5, 10, 15, 20}
	templ := ``

	for _, v := range Cantidades {
		if Muestra == v {
			templ += `<option value="` + strconv.Itoa(v) + `" selected>` + strconv.Itoa(v) + `</option>`
		} else {
			templ += `<option value="` + strconv.Itoa(v) + `">` + strconv.Itoa(v) + `</option>`
		}
	}
	return templ
}

//CargaComboCatalogoGrid recibe la clave del catálogo, el identificador opcional
//y regresa el template del combo del catálogo con el identificador seleccionado si así se desea
func CargaComboCatalogoGrid(Clave int) string {
	templ := ``
	templ = `:TODOS`
	templ += `;`

	Catalogo, _ := CatalogoModel.GetEspecificByFields("Clave", int64(Clave))
	for _, v := range Catalogo.Valores {
		templ += v.Valor + `:` + v.Valor
		templ += `;`
	}

	templ = templ[0 : len(templ)-1]
	return templ
}

//CargaComboCatalogo recibe la clave del catálogo, el identificador opcional
//y regresa el template del combo del catálogo con el identificador seleccionado si así se desea
func CargaComboCatalogo(Clave int, ID string) string {

	templ := ``

	if ID != "" {
		templ = `<option value="">--SELECCIONE--</option>`
	} else {
		templ = `<option value="" selected>--SELECCIONE--</option>`
	}

	if Clave == 0 {
		Catalogos, _ := CatalogoModel.GetAll()
		for _, v := range *Catalogos {
			if ID == v.ID.Hex() {
				templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Nombre + `</option>`
			} else {
				templ += `<option value="` + v.ID.Hex() + `">` + v.Nombre + `</option>`
			}
		}
	} else {
		Catalogo, _ := CatalogoModel.GetEspecificByFields("Clave", int64(Clave))
		for _, v := range Catalogo.Valores {
			if ID == v.ID.Hex() {
				templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Valor + `</option>`
			} else {
				templ += `<option value="` + v.ID.Hex() + `">` + v.Valor + `</option>`
			}
		}
	}
	return templ
}

//CargaComboCatalogo2 recibe la clave del catálogo, el identificador opcional
//y regresa el template del combo del catálogo con el identificador seleccionado si así se desea
func CargaComboCatalogo2(Clave int, nombre string) string {

	templ := ``

	if nombre != "" {
		templ = `<option value="">--SELECCIONE--</option>`
	} else {
		templ = `<option value="" selected>--SELECCIONE--</option>`
	}

	if Clave == 0 {
		Catalogos, _ := CatalogoModel.GetAll()
		for _, v := range *Catalogos {
			if nombre == v.Nombre {
				templ += `<option value="` + v.Nombre + `" selected>` + v.Nombre + `</option>`
			} else {
				templ += `<option value="` + v.Nombre + `">` + v.Nombre + `</option>`
			}
		}
	} else {
		Catalogo, _ := CatalogoModel.GetEspecificByFields("Clave", int64(Clave))
		for _, v := range Catalogo.Valores {
			if nombre == v.Valor {
				templ += `<option value="` + v.Valor + `" selected>` + v.Valor + `</option>`
			} else {
				templ += `<option value="` + v.Valor + `">` + v.Valor + `</option>`
			}
		}
	}
	return templ
}

//CargaComboUnidades recibe un Id en caso de solicitar combo seleccionado
func CargaComboUnidades(ID string) string {
	unidades, _ := UnidadModel.GetAll()
	templ := ``

	if ID != "" {
		templ = `<option value="">--SELECCIONE--</option>`
	} else {
		templ = `<option value="" selected>--SELECCIONE--</option>`
	}

	opc := ``

	for _, val := range *unidades {
		if ID == val.ID.Hex() {
			opc += `<option value="` + val.ID.Hex() + `" selected>` + val.Abreviatura + `</option>`
		} else {
			opc += `<option value="` + val.ID.Hex() + `">` + val.Abreviatura + `</option>`
		}
	}

	templ = templ + opc
	return templ
}

//CargaComboUnidadesEspecifico recibe un arreglo de ides de unidades y un id default.
func CargaComboUnidadesEspecifico(Default bson.ObjectId) string {
	opc := ``
	unidades, _ := UnidadModel.GetAll()
	for _, v := range *unidades {
		if Default == v.ID {
			opc += `<option value="` + v.ID.Hex() + `" selected>` + v.Abreviatura + `</option>`
		} else {
			opc += `<option value="` + v.ID.Hex() + `">` + v.Abreviatura + `</option>`
		}
	}
	return opc
}

//CargaComboCatalogoMulti Carga el combo para un multi select
func CargaComboCatalogoMulti(Clave int, ID string) string {

	Catalogo, _ := CatalogoModel.GetEspecificByFields("Clave", int64(Clave))

	templ := ``

	for _, v := range Catalogo.Valores {
		if ID == v.ID.Hex() {
			templ += `<option value="` + v.ID.Hex() + `" selected>` + v.Valor + `</option>`
		} else {
			templ += `<option value="` + v.ID.Hex() + `">` + v.Valor + `</option>`
		}
	}
	return templ
}

//CargaEstatusActivoEnAlta Extrae el Object Id del status 'ACTIVO' de un catalogo
func CargaEstatusActivoEnAlta(Clave int) bson.ObjectId {

	Catalogo, _ := CatalogoModel.GetEspecificByFields("Clave", int64(Clave))

	var objectid bson.ObjectId

	for _, v := range Catalogo.Valores {

		if v.Valor == "ACTIVO" {
			objectid = v.ID
		}
	}

	return objectid

}

//CargaCatalogoByID Extrae el Valor deseado de un catalgo desde el numero de catalogo y el id del valor deseado
func CargaCatalogoByID(Clave int, id bson.ObjectId) string {
	Catalogo, _ := CatalogoModel.GetEspecificByFields("Clave", int64(Clave))
	var cadena string
	for _, v := range Catalogo.Valores {
		if v.ID == id {
			cadena = v.Valor
		}
	}
	return cadena
}

//ArrayStringToObjectID Convierte un Arreglo de String a uno de Objects Ids
func ArrayStringToObjectID(ArrayStr []string) []bson.ObjectId {
	var ArrayID []bson.ObjectId
	for _, d := range ArrayStr {
		if bson.IsObjectIdHex(d) {
			ArrayID = append(ArrayID, bson.ObjectIdHex(d))
		}
	}
	return ArrayID
}
