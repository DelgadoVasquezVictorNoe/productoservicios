package MoConexion

import (
	"context"
	"errors"
	"fmt"

	"../../Modulos/Variables"
	elastic "gopkg.in/olivere/elastic.v5"
)

//DataE es una estructura que contiene los datos de configuración en el archivo cfg
var DataE = MoVar.CargaSeccionCFG(MoVar.SecElastic)

//ctx Contexto
var ctx = context.Background()

//Client variable global de Cliente en Elastic
var Client *elastic.Client

//ElasticConnection estructura que contiene una Sesión en Mgo.
type ElasticConnection struct {
	Client *elastic.Client
}

//ElasticNewClient abre un nuevo cliente en Elastic.
func ElasticNewClient(Server string) error {
	var err error
	Client, err = elastic.NewClient(elastic.SetURL(Server))
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

//ElasticGetclient obtiene el cliente abierto o abre uno nuevo en el Servidor Default.
func ElasticGetclient() (*elastic.Client, error) {
	if Client == nil {
		err := ElasticNewClient(DataE.BaseURL)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return Client, nil
}

//ElasticGetclientServer obtiene el cliente abierto o abre uno nuevo especificando el Servidor.
func ElasticGetclientServer(Server string) (*elastic.Client, error) {
	if Client == nil {
		err := ElasticNewClient(Server)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return Client, nil
}

//ElasticNewConnection crea una copia del cliente existente o de uno nuevo, si no existe.
func ElasticNewConnection() (*ElasticConnection, error) {
	client, err := ElasticGetclient()
	if err != nil {
		return nil, err
	}
	context := &ElasticConnection{
		Client: client,
	}
	return context, nil
}

//ElasticNewConnectionToServer crea una copia del cliente existente o de uno nuevo, si no existe.
func ElasticNewConnectionToServer(Server string) (*ElasticConnection, error) {
	client, err := ElasticGetclientServer(Server)
	if err != nil {
		return nil, err
	}
	context := &ElasticConnection{
		Client: client,
	}
	return context, nil
}

//####################################################################################################

//CheckIndex verifica que el nombre del índice exista en elastic server, para evitar errores no controlados.
func (e ElasticConnection) CheckIndex(Name string) (bool, error) {
	Nombre := Name
	if Nombre == "" {
		Nombre = MoVar.IndexElastic
	}
	exists, err := e.Client.IndexExists(Nombre).Do(ctx)
	if err != nil {
		fmt.Println("Error al verificar index de elastic", err)
		return false, err
	}
	if !exists {
		fmt.Printf("\n El Indice: %s no existe ", Name)
		return false, nil
	}
	return true, nil
}

//ElasticInserta inserta un documento en el index específico del sistema especificando además Tipo y Id.
func ElasticInserta(Index, Type, ID string, Data interface{}) error {
	Indice := Index
	if Indice == "" {
		Indice = MoVar.IndexElastic
	}
	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Indice)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return err
	}
	if existe {
		Put, err := conn.Client.Index().Index(Indice).Type(Type).Id(ID).BodyJson(Data).Do(ctx)
		if err != nil {
			fmt.Println("Error al obtener el cliente de elastic ", err)
			return err
		}
		fmt.Printf("\nIndexado documento con id: %s en el index %s, y type %s\n", Put.Id, Put.Index, Put.Type)
		return nil
	}
	return errors.New("El Índice : " + Indice + " no existe")
}

//ElasticActualiza  actualiza correctamente un documento en elasticsearch
func ElasticActualiza(Index, Type, ID string, Data interface{}) error {
	Indice := Index
	if Indice == "" {
		Indice = MoVar.IndexElastic
	}

	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Indice)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return err
	}
	if existe {
		_, err = conn.Client.Update().Index(Indice).Type(Type).Id(ID).Doc(Data).DetectNoop(true).Do(context.TODO())
		if err != nil {
			fmt.Println("Error al Actualizar en elasticSearch", err)
			return err
		}
		return nil
	}
	return errors.New("El Índice : " + Indice + " no existe")
}

//ElasticDeleteID elimina un docuemnto de elastic por ID
func ElasticDeleteID(Index, Type, ID string) error {
	Indice := Index
	if Indice == "" {
		Indice = MoVar.IndexElastic
	}
	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Indice)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return err
	}
	if existe {
		_, err = conn.Client.Get().Index(Index).Type(Type).Id(ID).Do(ctx)
		if err != nil {
			fmt.Println(err)
			return err
		}
		_, err = conn.Client.Delete().Index(Indice).Type(Type).Id(ID).Do(ctx)
		if err != nil {
			fmt.Println(err)
			return err
		}
		return nil
	}
	return errors.New("El Índice: " + Indice + " no existe")
}

//ElasticConsultaExistencia consulta un documento de elastic por ID
func ElasticConsultaExistencia(Index, Type, ID string) (bool, error) {
	Indice := Index
	if Indice == "" {
		Indice = MoVar.IndexElastic
	}

	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return false, err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Indice)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return false, err
	}
	if existe {
		doc, err := conn.Client.Get().Index(Indice).Type(Type).Id(ID).Do(ctx)
		if err != nil {
			fmt.Println(err)
			return false, err
		}
		if doc.Found {
			return true, nil
		}
		return false, nil
	}
	return false, errors.New("El Índice: " + Indice + " no existe")
}

//ElasticBuscaEspecifica busca documentos por un texto dado, especificando indice, tipo, rango y tipo de consulta
func ElasticBuscaEspecifica(Index, Type string, From, Size int, consulta *elastic.QueryStringQuery) (*elastic.SearchResult, error) {
	Indice := Index
	if Indice == "" {
		Indice = MoVar.IndexElastic
	}

	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return nil, err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Indice)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return nil, err
	}

	if existe {
		docs, err := conn.Client.Search().Index(Indice).Type(Type).Query(consulta).From(From).Size(Size).Do(ctx)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
		return docs, nil
	}
	return nil, errors.New("El Índice: " + Indice + " no existe")
}

//ElasticBusquedaDefault realiza una consulta básica (10 reg) al servidor de elastic.
func ElasticBusquedaDefault(Index, Type string, consulta *elastic.QueryStringQuery) (*elastic.SearchResult, error) {
	Indice := Index
	if Indice == "" {
		Indice = MoVar.IndexElastic
	}

	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return nil, err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Indice)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return nil, err
	}
	if existe {
		docs, err := conn.Client.Search().Index(Indice).Type(Type).Query(consulta).Do(ctx)
		if err != nil {
			return nil, err
		}

		return docs, nil
	}
	return nil, errors.New("El Índice: " + Indice + " no existe")
}

//ElasticFlush hace flush a determinado index de elastic
func ElasticFlush(Index string) error {
	Indice := Index
	if Indice == "" {
		Indice = MoVar.IndexElastic
	}

	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Indice)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return err
	}
	if existe {
		_, err = conn.Client.Flush().Index(Indice).Do(ctx)
		if err != nil {
			fmt.Println(err)
			return err
		}
		return nil

	}
	return errors.New("El Índice: " + Indice + " no existe")
}

//ElasticConsultaIndexSat consulta un index completo de leastic
func ElasticConsultaIndexSat(Index, tipo, valor, campo string) (*elastic.SearchResult, error) {
	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return nil, err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Index)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return nil, err
	}
	if existe {

		var docs *elastic.SearchResult
		var err2 error

		if valor != "" {

			consulta := elastic.NewQueryStringQuery(valor)
			consulta = consulta.Field(campo)

			switch campo {
			case "codigofamilia":
				docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).Sort("familianumerico", true).From(0).Size(10000).Do(ctx)
			case "codigosegmento":
				docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).Sort("segmentonumerico", true).From(0).Size(10000).Do(ctx)
			case "codigoclase":
				docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).Sort("clasenumerico", true).From(0).Size(10000).Do(ctx)
			default:
				docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).From(0).Size(10000).Do(ctx)
			}

		} else {
			consulta := elastic.NewMatchAllQuery()

			switch campo {
			case "codigofamilia":
				docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).Sort("familianumerico", true).From(0).Size(10000).Do(ctx)
			case "codigosegmento":
				docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).Sort("segmentonumerico", true).From(0).Size(10000).Do(ctx)
			case "codigoclase":
				docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).Sort("clasenumerico", true).From(0).Size(10000).Do(ctx)
			default:
				docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).From(0).Size(10000).Do(ctx)
			}

		}

		if err2 != nil {
			return nil, err2
		}
		return docs, nil
	}
	return nil, errors.New("El Índice: " + Index + " no existe")
}

//ElasticGetEspecificByFields consulta un index completo de leastic
func ElasticGetEspecificByFields(Index, tipo, valor, campo string) (*elastic.SearchResult, error) {
	conn, err := ElasticNewConnection()
	if err != nil {
		fmt.Println("Error al obtener el cliente: ", err)
		return nil, err
	}
	defer conn.Client.Stop()

	existe, err := conn.CheckIndex(Index)
	if err != nil {
		fmt.Println("Error al consultar existencia de índice: ", err)
		return nil, err
	}
	if existe {

		var docs *elastic.SearchResult
		var err2 error

		if valor != "" {

			consulta := elastic.NewQueryStringQuery(valor)
			consulta = consulta.Field(campo)

			//docs, err2 = conn.Client.Search().Index(Index).Type(Tipo).Query(consulta).Sort(campo, true).From(0).Size(10000).Do(ctx)

			docs, err2 = conn.Client.Search().Index(Index).Type(tipo).Query(consulta).From(0).Size(1).Do(ctx)
			//conn.Client.Search().Index(Indice).Type(Type).Query(consulta).Do(ctx)
			/*switch campo {
			case "codigofamilia":
				docs, err2 = conn.Client.Search().Index(Index).Type("Producto").Query(consulta).Sort("familianumerico", true).From(0).Size(10000).Do(ctx)
			case "codigosegmento":
				docs, err2 = conn.Client.Search().Index(Index).Type("Producto").Query(consulta).Sort("segmentonumerico", true).From(0).Size(10000).Do(ctx)
			case "codigoclase":
				docs, err2 = conn.Client.Search().Index(Index).Type("Producto").Query(consulta).Sort("clasenumerico", true).From(0).Size(10000).Do(ctx)
			default:
				docs, err2 = conn.Client.Search().Index(Index).Type("Producto").Query(consulta).From(0).Size(10000).Do(ctx)
			}*/

		}
		if err2 != nil {
			return nil, err2
		}
		return docs, nil
	}
	return nil, errors.New("El Índice: " + Index + " no existe")
}
