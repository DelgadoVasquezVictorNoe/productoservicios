package MoConexion

import (
	"fmt"
	"os"

	"../../Modulos/Variables"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//DataM es una estructura que contiene los datos de configuración en el archivo cfg
var DataM = MoVar.CargaSeccionCFG(MoVar.SecMongo)

//Session variable global de sesión en Mgo
var Session *mgo.Session

//MongoConnection estructura que contiene una Sesión en Mgo.
type MongoConnection struct {
	Session *mgo.Session
}

//MongoNewSession abre una nueva sesión en Mongo especificando el Servidor
func MongoNewSession(Server string) error {
	var err error
	Session, err = mgo.Dial(Server)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

//MongoGetSession obtiene la sesión iniciada en Mgo o crea una nueva en el servidor Default
func MongoGetSession() (*mgo.Session, error) {
	if Session == nil {
		err := MongoNewSession(DataM.Servidor)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return Session, nil
}

//MongoGetSessionServer obtiene la sesión iniciada en Mgo o crea una nueva especificando el Servidor
func MongoGetSessionServer(Server string) (*mgo.Session, error) {
	if Session == nil {
		err := MongoNewSession(Server)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return Session, nil
}

//MongoNewConnection crea una copia de la sesión existente o de una nueva si no existe.
func MongoNewConnection() (*MongoConnection, error) {
	session, err := MongoGetSession()
	if err != nil {
		return nil, err
	}
	context := &MongoConnection{
		Session: session.Copy(),
	}
	return context, nil
}

//MongoNewConnectionToServer crea una copia de la sesión existente o de una nueva si no existe.
func MongoNewConnectionToServer(Server string) (*MongoConnection, error) {
	session, err := MongoGetSessionServer(Server)
	if err != nil {
		return nil, err
	}
	context := &MongoConnection{
		Session: session.Copy(),
	}
	return context, nil
}

//########################################################

//Close método propio de Context para cerrar la Sesión de Mgo que contiene.
func (c *MongoConnection) Close() {
	c.Session.Close()
}

//GetCollectionFromBase regresa una colección de Mgo especificando el nombre de la Base y la Colección.
func (c *MongoConnection) GetCollectionFromBase(DB, C string) *mgo.Collection {
	return c.Session.DB(DB).C(C)
}

//GetCollection regresa una colección de Mgo especificando el nombre de la Base y la Colección.
func (c *MongoConnection) GetCollection(C string) *mgo.Collection {
	return c.Session.DB(DataM.NombreBase).C(C)
}

//GetBase regresa una base de Mgo especificando el nombre de la Base.
func (c *MongoConnection) GetBase(DB string) *mgo.Database {
	c.Session.SetMode(mgo.Monotonic, true)
	return c.Session.DB(DB)
}

//########################################################

//MongoUploadFile Inserta un Archivo en Mongo y regresa el string del Id del archivo
func MongoUploadFile(path, namefile string) (string, error) {
	Conn, err := MongoNewConnection()
	if err != nil {
		return "", err
	}
	defer Conn.Close()

	Db := Conn.GetBase(DataM.NombreBase)

	file2, err := os.Open(path + "/" + namefile)
	if err != nil {
		return "", err
	}
	defer file2.Close()

	stat, err := file2.Stat()
	if err != nil {
		return "", err
	}

	bs := make([]byte, stat.Size())
	_, err = file2.Read(bs)
	if err != nil {
		return "", err
	}

	img, err := Db.GridFS(MoVar.ArchivosCollection).Create(namefile)
	if err != nil {
		return "", err
	}

	idsImg := img.Id()
	_, err = img.Write(bs)
	if err != nil {
		return "", err
	}

	err = img.Close()
	if err != nil {
		return "", err
	}

	idimg := idsImg.(bson.ObjectId)

	return idimg.Hex(), nil
}

//MongoGetFile regresa un archivo GridFile de -Mongo especificando el Id
func MongoGetFile(ID bson.ObjectId) (*mgo.GridFile, error) {

	var File *mgo.GridFile
	Conn, err := MongoNewConnection()
	if err != nil {
		return nil, err
	}
	defer Conn.Close()

	Db := Conn.GetBase(DataM.NombreBase)

	img, err := Db.GridFS(MoVar.ArchivosCollection).OpenId(ID)
	if err != nil {
		return File, err
	}

	b := make([]byte, img.Size())
	_, err = img.Read(b)
	if err != nil {
		return File, err
	}
	return File, nil
}
