package MoConexion

import (
	"database/sql"
	"fmt"

	"../../Modulos/Variables"
)

//PsqlConfig estructura que contiene los datos de conexion para postgres
type PsqlConfig struct {
	Servidor   string
	Usuario    string
	Pass       string
	NombreBase string
}

//DataP es una estructura que contiene los datos de configuración en el archivo cfg
var DataP = MoVar.CargaSeccionCFG(MoVar.SecPsql)

//PsqlDB variable global de conexión a Base de Datos en Sql
var PsqlDB *sql.DB

//PsqlConnection estructura global de conexión a Base de Datos en Sql
type PsqlConnection struct {
	PsqlDB *sql.DB
}

//DefaultConfig es la configuración default de la conexión a base de datos en CFG
var DefaultConfig = PsqlConfig{
	DataP.Servidor,
	DataP.Usuario,
	DataP.Pass,
	DataP.NombreBase,
}

//PsqlNewDB abre una Base en Psql con servidor.
func PsqlNewDB(Config PsqlConfig) error {
	var err error
	dbinfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable", Config.Servidor, Config.Usuario, Config.Pass, Config.NombreBase)
	PsqlDB, err = sql.Open("postgres", dbinfo)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

//PsqlGetDB obtiene una Base abierta o abre una nueva en Psql con servidor.
func PsqlGetDB(Config PsqlConfig) (*sql.DB, error) {
	if PsqlDB == nil {
		err := PsqlNewDB(Config)
		if err != nil {
			fmt.Println(err)
			return nil, err
		}
	}
	return PsqlDB, nil
}

//PsqlNewConnection crea una copia de la DB existente o de uno nuevo, si no existe.
func PsqlNewConnection(Config PsqlConfig) (*PsqlConnection, error) {
	db, err := PsqlGetDB(Config)
	if err != nil {
		return nil, err
	}
	context := &PsqlConnection{
		PsqlDB: db,
	}
	return context, nil
}

//PsqlDefaultConnection crea una copia de la DB existente o de uno nuevo, si no existe.
func PsqlDefaultConnection() (*PsqlConnection, error) {
	db, err := PsqlGetDB(DefaultConfig)
	if err != nil {
		return nil, err
	}
	context := &PsqlConnection{
		PsqlDB: db,
	}
	return context, nil
}

//Ping hace ping a una conexión específica.
func (p *PsqlConnection) Ping() error {
	err := p.PsqlDB.Ping()
	if err != nil {
		return err
	}
	return nil
}

//Begin inicia una istancia de BD para realizar una transacción.
func (p *PsqlConnection) Begin() (*sql.Tx, error) {
	tx, err := p.PsqlDB.Begin()
	if err != nil {
		return nil, err
	}
	return tx, nil
}

//Close una conexión a base de datos específica.
func (p *PsqlConnection) Close() error {
	err := p.PsqlDB.Close()
	if err != nil {
		return err
	}
	return nil
}

//Prepare prepara un statement para correr una query.
func (p *PsqlConnection) Prepare(Query string) (*sql.Stmt, error) {
	Stmt, err := p.PsqlDB.Prepare(Query)
	if err != nil {
		return nil, err
	}
	return Stmt, nil
}
