package MoGeneral

import (
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"

	iris "gopkg.in/kataras/iris.v6"
	"gopkg.in/mgo.v2/bson"

	"../../Modulos/Conexiones"
	"../../Modulos/Crypto"
	"../../Modulos/Estructuras"
	"../../Modulos/Variables"
	"../../Modulos/WebService"
)

var dataM = MoVar.CargaSeccionCFG(MoVar.SecMongo)

//########### GENERALES #######################################

//EstaVacio verifica si un objeto está vacío o no
func EstaVacio(object interface{}) bool {
	if object == nil {
		return true
	} else if object == "" {
		return true
	} else if object == false {
		return true
	}

	if reflect.ValueOf(object).Kind() == reflect.Struct {
		empty := reflect.New(reflect.TypeOf(object)).Elem().Interface()
		if reflect.DeepEqual(object, empty) {
			return true
		}
	}
	return false
}

//CheckStruct reflects struct variables and its values and print them
func CheckStruct(object interface{}) {
	fmt.Println("#################### CHEKING OBJECT ######################")
	if reflect.ValueOf(object).Kind() == reflect.Struct {
		s := reflect.New(reflect.TypeOf(object)).Elem()
		typeOfT := s.Type()
		fmt.Println()
		fmt.Println("#### STRUCT ####")
		fmt.Println(typeOfT.String(), " ", s.Kind().String(), "{")
		for i := 0; i < s.NumField(); i++ {
			f := s.Field(i)
			fmt.Printf("   %s   %s =  %v\n", typeOfT.Field(i).Name, f.Type(), reflect.ValueOf(object).Field(i).Interface())
		}
		fmt.Println("}")
	} else {
		fmt.Println(" Object is ", reflect.ValueOf(object).Kind().String(), " Not a Struct.")
	}
	fmt.Println("#################### OBJECT CHECKED ######################")
}

//ConstruirCadenas recibe un texto y regresa dos que se utilizarán para buscar en elastic
func ConstruirCadenas(texto string) (string, string) {

	var palabras = []string{}
	var final = []string{}
	var final2 = []string{}
	var cadenafinal string
	var cadenafinal2 string

	nuevacadena := strings.Replace(texto, "/", "\\/", -1)
	nuevacadena = strings.Replace(nuevacadena, "~", "\\~", -1)
	nuevacadena = strings.Replace(nuevacadena, "^", "\\^", -1)
	nuevacadena = strings.Replace(nuevacadena, "+", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "[", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "]", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "{", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "}", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "(", "\\(", -1)
	nuevacadena = strings.Replace(nuevacadena, ")", "\\)", -1)
	nuevacadena = strings.Replace(nuevacadena, "|", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "=", "", -1)
	nuevacadena = strings.Replace(nuevacadena, ">", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "<", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "!", "", -1)
	nuevacadena = strings.Replace(nuevacadena, "&", "", -1)

	palabras = strings.Split(nuevacadena, " ")

	for _, valor := range palabras {
		if valor != "" {
			palabrita := valor + "~1"
			final = append(final, palabrita)
		}
	}

	for _, valor := range palabras {
		if valor != "" {
			palabrita := `+` + `"` + valor + `"`
			final2 = append(final2, palabrita)
		}
	}

	for _, value := range final {
		cadenafinal = cadenafinal + " " + value
	}

	for _, value := range final2 {
		cadenafinal2 = cadenafinal2 + " " + value
	}

	return cadenafinal, cadenafinal2
}

//MiURI retorna la uri a la cual se hace la peticion, sin parametros
func MiURI(URI, ID string) string {
	arr := strings.Split(URI, "/")
	nuevaURI := ""
	for _, val := range arr {
		if val != ID && val != "" {
			// fmt.Println("[", i, "]", "=", val)
			nuevaURI += "/" + val
		}
	}
	fmt.Println(nuevaURI)
	return nuevaURI
}

//EliminarEspaciosInicioFinal Elimina los espacios en blanco Al inicio y final de una cadena:
//recibe cadena, regresa cadena limpia de espacios al inicio o final o "" si solo contiene espacios
func EliminarEspaciosInicioFinal(cadena string) string {
	var cadenalimpia string
	cadenalimpia = cadena
	re := regexp.MustCompile("(^\\s+|\\s+$)")
	cadenalimpia = re.ReplaceAllString(cadenalimpia, "")
	return cadenalimpia
}

// SelectDistinctFromSliceString returns a unique subset of the string slice provided.
func SelectDistinctFromSliceString(input []string) []string {
	u := make([]string, 0, len(input))
	m := make(map[string]bool)

	for _, val := range input {
		if _, ok := m[val]; !ok {
			m[val] = true
			u = append(u, val)
		}
	}

	return u
}

//CreaMapaDeSliceString crea un mapa de string string dados dos slices de string
func CreaMapaDeSliceString(Campo1, Campo2 []string) map[string]string {
	m := make(map[string]string)

	for k, v := range Campo1 {
		m[v] = Campo2[k]
	}

	return m
}

//GeneraModeloColumnas genera un mapa con el modelo de las columnas del Index
//a partir de las opciones propias de cada modelo
func GeneraModeloColumnas(Opciones map[string]interface{}) []map[string]interface{} {
	var result []map[string]interface{}

	for k, v := range Opciones["name"].([]string) {
		var Mapa map[string]interface{}
		Mapa = make(map[string]interface{})
		Mapa["name"] = v
		Mapa["index"] = Opciones["index"].([]string)[k]
		Mapa["align"] = Opciones["align"].([]string)[k]
		Mapa["sorteable"] = Opciones["sorteable"].([]bool)[k]
		Mapa["search"] = Opciones["search"].([]bool)[k]
		Mapa["editable"] = Opciones["editable"].([]bool)[k]
		Mapa["hidden"] = Opciones["hidden"].([]bool)[k]
		Mapa["stype"] = Opciones["stype"].([]string)[k]

		Mapa["sorttype"] = Opciones["sorttype"].([]string)[k]
		if Opciones["sorttype"].([]string)[k] == "date" {
			Mapa["formatter"] = "y/m/d"
			Mapa["srcformat"] = "y/m/d"
			Mapa["newformat"] = "y/j/n"
		}

		Mapa["searchoptions"] = Opciones["searchoptions"].([]interface{})[k]

		result = append(result, Mapa)
	}

	return result
}

//GeneraDataRowDeIndex crea templates de tabla de búsqueda
func GeneraDataRowDeIndex(Funcion, Ides string) map[string]interface{} {
	conn, err := MoConexion.MongoNewConnection()
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer conn.Close()

	var resultado map[string]interface{}
	conn.Session.SetSocketTimeout(time.Duration(5 * time.Minute))

	if Ides != "" {
		err = conn.Session.DB(dataM.NombreBase).Run(bson.M{"eval": fmt.Sprintf(`%s(%s);`, Funcion, Ides)}, &resultado)
	} else {
		err = conn.Session.DB(dataM.NombreBase).Run(bson.M{"eval": fmt.Sprintf(`%s([]);`, Funcion)}, &resultado)
	}
	if err != nil {
		fmt.Println(err)
		return nil
	}

	var Mapa map[string]interface{}
	Mapa = make(map[string]interface{})

	if resultado != nil {
		Datos := resultado["retval"]
		Mapa["rows"] = Datos
	}

	return Mapa
}

//Totalpaginas calcula el número de paginaciones de acuerdo al número
// de resultados encontrados y los que se quieren mostrar en la página.
func Totalpaginas(numeroRegistros int, limitePorPagina int) int {
	NumPagina := float32(numeroRegistros) / float32(limitePorPagina)
	NumPagina2 := int(NumPagina)
	if NumPagina > float32(NumPagina2) {
		NumPagina2++
	}
	return NumPagina2
}

//ConstruirPaginacion2 construtye la paginación en formato html para usarse en la página
func ConstruirPaginacion2(paginasTotales int, pag int) string {
	var lt int
	var rt int

	lt = 1
	rt = paginasTotales

	if pag > 2 {
		lt = pag - 1
	}
	if paginasTotales > pag {
		rt = pag + 1
	}

	//inicio
	var templateP string

	templateP = `

    <div class="input-group col-md-8">
      
	  <span class="input-group-btn" onclick="BuscaPaginaSat(1)">
        <button class="btn btn-primary" type="button"><span aria-hidden="true">&laquo;</span></button>
      </span>
      
	  <span class="input-group-btn" onclick="BuscaPaginaSat(` + strconv.Itoa(lt) + `)">
        <button class="btn btn-info" type="button"><span aria-hidden="true">&lt;</span></button>
      </span>`

	if paginasTotales == 0 {
		templateP += `<input class="form-control" onkeypress="checkSubmit(this))" id="inputPagB" onchange="BuscaPaginaSat(this.value)" value="` + strconv.Itoa(paginasTotales) + `" type="number" name="paginasat" min="0" max="1">`
	} else {
		templateP += `<input class="form-control" onkeypress="checkSubmit(this))" id="inputPagB" onchange="BuscaPaginaSat(this.value)" value="` + strconv.Itoa(pag) + `" type="number" name="paginasat" min="1" max="` + strconv.Itoa(paginasTotales) + `">`
	}

	templateP += `
	  <span class="input-group-btn">
        <button class="btn btn-default" type="button"> de ` + strconv.Itoa(paginasTotales) + `</button>
      </span>

      <span class="input-group-btn"  onclick="BuscaPaginaSat(` + strconv.Itoa(rt) + `)">
        <button class="btn btn-info" type="button"><span aria-hidden="true">&gt;</span></button>
      </span>
      <span class="input-group-btn" onclick="BuscaPaginaSat(` + strconv.Itoa(paginasTotales) + `)">
        <button class="btn btn-primary" type="button"><span aria-hidden="true">&raquo;</span></button>
      </span>
    </div>
`
	return templateP
}

//CargaComboMostrarEnIndex carga las opciones de mostrar en el index
func CargaComboMostrarEnIndex(Muestra int) string {
	var Cantidades = []int{5, 10, 15, 20}
	templ := ``

	for _, v := range Cantidades {
		if Muestra == v {
			templ += `<option value="` + strconv.Itoa(v) + `" selected>` + strconv.Itoa(v) + `</option>`
		} else {
			templ += `<option value="` + strconv.Itoa(v) + `">` + strconv.Itoa(v) + `</option>`
		}
	}
	return templ
}

//DocErrorToSend construye un docuemnto a enviar con repuesta para error
func DocErrorToSend(Status bool, Process, From, Msg string) MoEstructuras.DocAnswer {
	var Error MoEstructuras.DocAnswer
	Error.Header.Status = Status
	Error.Header.Msg = Msg
	Error.Header.From = From
	Error.Header.Date = time.Now()
	Error.Header.Process = Process

	Error.Body.Answer = Status
	Error.Body.Msg = Msg

	return Error
}

//GetDocErrorToSend crea y regresa un documento de error al que consume el web service
func GetDocErrorToSend(Status bool, Process, From, Msg string) (MoEstructuras.Send, error) {
	Send, err := Crypto.CodificaSend(DocErrorToSend(Status, Process, From, Msg))
	if err != nil {
		fmt.Println(err)
		return Send, err
	}
	return Send, nil
}

//PrepareDocRequest lee, decodifica y convierte a json el request (peticion) del consumidor.
func PrepareDocRequest(ctx *iris.Context) (MoEstructuras.DocAsk, error) {
	var Doc MoEstructuras.DocAsk
	// ctx.ResponseWriter.StatusCode() Agregar validacin para leer la cabecera.
	Send, err := WebService.ReadRequestToSend(ctx.Request)
	if err != nil {
		return Doc, err
	}

	DataBytes, err := Crypto.DecodificaSend(Send)
	if err != nil {
		return Doc, err
	}

	var Document MoEstructuras.DocAsk
	err = json.Unmarshal(DataBytes, &Document)
	if err != nil {
		return Doc, err
	}

	return Document, nil
}
