package MoVar

import (
	config "github.com/robfig/config"
)

const (

	//############# ARCHIVOS LOCALES ######################################

	//FileConfigName contiene el nombre del archivo CFG
	FileConfigName = "Acfg.cfg"

	//CarpetaDeImagenes carpeta para imagenes
	CarpetaDeImagenes = "./Archivos/Imagenes"

	//ArchivosCollection nombre de la coleccion para guardar archivos en  Mongo
	ArchivosCollection = "Imagenes"

	//################ SECCIONES CFG  ######################################

	//SecDefault nombre de la seccion default del servidor en CFG
	SecDefault = "DEFAULT"
	//SecMongo nombre de la seccion de mongo en CFG
	SecMongo = "CONFIG_DB_MONGO"
	//SecPsql nombre de la seccion de postgresql en cfg
	SecPsql = "CONFIG_DB_POSTGRES"
	//SecElastic nombre de la seccion de postgresql en cfg
	SecElastic = "CONFIG_DB_ELASTIC"

	//############# COLUMNAS DE ALMACEN PSQL ######################################

	//############# COLECCIONES MONGO ######################################

	//BaseDatosMongo nombre del index a usar en elastic
	BaseDatosMongo = "MinisuperAmpliada"

	//Mongo---------------> Catalogo

	//ColeccionCatalogo nombre de la coleccion de Catalogo en mongo
	ColeccionCatalogo = "Catalogo"

	//Mongo---------------> Unidad

	//ColeccionUnidad nombre de la coleccion de Unidad en mongo
	ColeccionUnidad = "Unidad"

	//Mongo---------------> Cliente

	//ColeccionCliente nombre de la coleccion de Cliente en mongo
	ColeccionCliente = "Cliente"

	//Mongo---------------> Producto

	//ColeccionProducto nombre de la coleccion de Producto en mongo
	ColeccionProducto = "Producto"

	//Mongo---------------> MediosPago

	//ColeccionMediosPago nombre de la coleccion de MediosPago en mongo
	ColeccionMediosPago = "MediosPago"

	//Mongo---------------> Persona

	//ColeccionPersona nombre de la coleccion de Persona en mongo
	ColeccionPersona = "Persona"

	//Mongo---------------> Usuario

	//ColeccionUsuario nombre de la coleccion de Usuario en mongo
	ColeccionUsuario = "Usuario"

	//Mongo---------------> Bug

	//ColeccionBug nombre de la coleccion de Bug en mongo
	ColeccionBug = "Bug"

	//##########################<CATÁLOGOS DEL SISTEMA>######################

	//################# DATOS ELASTIC ######################################

	//Elastic---------------> Catalogo

	//TipoCatalogo tipo a manejar en elastic
	TipoCatalogo = "Catalogo"

	//Elastic---------------> Unidad

	//TipoUnidad tipo a manejar en elastic
	TipoUnidad = "Unidad"

	//Elastic---------------> Cliente

	//TipoCliente tipo a manejar en elastic
	TipoCliente = "Cliente"

	//Elastic---------------> Producto

	//TipoProducto tipo a manejar en elastic
	TipoProducto = "Producto"

	//Elastic---------------> MediosPago

	//TipoMediosPago tipo a manejar en elastic
	TipoMediosPago = "MediosPago"

	//Elastic---------------> Persona

	//TipoPersona tipo a manejar en elastic
	TipoPersona = "Persona"

	//Elastic---------------> Usuario

	//TipoUsuario tipo a manejar en elastic
	TipoUsuario = "Usuario"

	//Elastic---------------> Bug

	//TipoBug tipo a manejar en elastic
	TipoBug = "Bug"

	//IndexElastic nombre del index a usar en elastic
	IndexElastic = "minisuperampliado"

	//IndexClasificador nombre del index a usar en elastic para clasificar productos con código del SAT
	IndexClasificador = "clasificadorsatfull"

	// * -----------------------CLAVES DE CATALOGO ------------------- * //

	// CatalogoDeEstatusDeCliente numero de catalogo en la coleccion Catalogos
	CatalogoDeEstatusDeCliente = 137
)

//DataCfg estructura de datos del entorno
type DataCfg struct {
	BaseURL    string
	Servidor   string
	Puerto     string
	Usuario    string
	Pass       string
	Protocolo  string
	NombreBase string
}

//#################<Funciones Generales>#######################################

//CargaSeccionCFG rellena los datos de la seccion a utilizar
func CargaSeccionCFG(seccion string) DataCfg {
	var d DataCfg
	var FileConfig, err = config.ReadDefault(FileConfigName)
	if err == nil {
		if FileConfig.HasOption(seccion, "baseurl") {
			d.BaseURL, _ = FileConfig.String(seccion, "baseurl")
		}
		if FileConfig.HasOption(seccion, "servidor") {
			d.Servidor, _ = FileConfig.String(seccion, "servidor")
		}
		if FileConfig.HasOption(seccion, "puerto") {
			d.Puerto, _ = FileConfig.String(seccion, "puerto")
		}
		if FileConfig.HasOption(seccion, "usuario") {
			d.Usuario, _ = FileConfig.String(seccion, "usuario")
		}
		if FileConfig.HasOption(seccion, "pass") {
			d.Pass, _ = FileConfig.String(seccion, "pass")
		}
		if FileConfig.HasOption(seccion, "protocolo") {
			d.Protocolo, _ = FileConfig.String(seccion, "protocolo")
		}
		if FileConfig.HasOption(seccion, "base") {
			d.NombreBase, _ = FileConfig.String(seccion, "base")
		}
	}
	return d
}
